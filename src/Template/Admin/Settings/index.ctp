<div class="ui error message" style="display:none;"></div>
<div class="ui info message" style="display:none;"></div>
<div class="ui form settings">
	<h4 class="ui dividing header"><?= __('Edit settings'); ?></h4>
	<div class="four fields">
		<div class="field"><input type="password" name="oldpassword" placeholder="<?= __('Old password'); ?>" maxlength="40" /></div>
		<div class="field"><input type="password" name="newpassword" placeholder="<?= __('New password'); ?>" maxlength="40" /></div>
		<div class="field"><input type="password" name="conpassword" placeholder="<?= __('Confirm again'); ?>" maxlength="40" /></div>
		<div class="field"><div class="ui button save-btn" cmd="change-password"><i class="check blue icon"></i><?= __('Change'); ?></div></div>
	</div>
</div>
<script type="text/javascript">
$('<script>').appendTo('div.settings').attr('src', '/js/Admin/settings/index.js');
</script>