<?php //debug($types); ?>
<?php foreach($types as $idx=>$type): ?>
<div class="field menu-options">
    <label><?= __($type['name']); ?> :</label>
    <select id="MenuOption<?= $type['id']; ?>" name="extsearch<?= $type['level']; ?>" class="dynamic-menu" cmdlevel="<?= $type['level']; ?>">
        <option value=""><?= __('Skip relating to {0}', [$type['name']]); ?></option>
        <?php if(isset($type['options'])): ?>
            <?php foreach($type['options'] as $option): ?>  
            <option value="<?= $option['name']; ?>" cmdid="<?= $option['id']; ?>">
                <?= $option['name']; ?>
            </option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>  
<?php endforeach; ?>
<?php if(isset($type['options'][0]['id'])): ?>
<script type="text/javascript">
//$.post('/admin/downloads/getsub/plain', {parentid:<?= $type['options'][0]['id']; ?> }, function(result){
//    if(result){
//        $('div.dynamic-fields select.dynamic-menu:gt(<?= $type['level'] - 1; ?>)').remove();
//        $('div.dynamic-fields').append(result);
//    }
//});
</script>
<?php endif; ?>