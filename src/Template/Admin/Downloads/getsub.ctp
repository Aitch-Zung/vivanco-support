<?php //debug($types); ?>
<?php foreach($types as $idx=>$type): ?>
<div class="column menu-options">
	<h4><?= __($type['name']); ?> 
        <select class="dynamic-menu" name="type-<?= $type['id']; ?>" cmdid="<?= $type['id']; ?>" cmdlevel="<?= $type['level']; ?>">
            <?php if(isset($type['options'])): ?>
                <?php foreach($type['options'] as $option): ?>  
                <option class="item" value="<?= $option['id']; ?>">
                    <?= $option['name']; ?>
                </option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="ui icon button tiny green help-popup" cmd="add-option" cmdid="<?= $type['id']; ?>" parentid="<?= $parentid; ?>" data-content="<?= __('Add 1 option / time'); ?>" data-variation="inverted"><i class="plus icon"></i></span>
        <span class="ui icon button tiny teal green help-popup" cmd="add-options" cmdid="<?= $type['id']; ?>" cmdtext="<?= $type['name']; ?>" parentid="<?= $parentid; ?>" data-content="<?= __('Add N options / time'); ?>" data-variation="inverted"><i class="list layout icon icon"></i></span>
    </h4>
	<div id="MenuOption<?= $type['id']; ?>" class="ui vertical menu" style="width: 95%;">
	<?php if(isset($type['options'])): ?>
		<?php foreach($type['options'] as $option): ?>	
		<div class="item">
			<?= $option['name']; ?>
			<span class="ui red tiny button label" cmd="delete-option" cmdid="<?= $option['id']; ?>" style="width:30px;"><i class="remove icon"></i></span>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div>	
<?php endforeach; ?>
<?php if(isset($type['options'][0]['id'])): ?>
<script type="text/javascript">
$.post('/admin/downloads/getsub', {parentid:<?= $type['options'][0]['id']; ?> }, function(result){
    if(result){
        $('div.option-cloumn-grid div.menu-options:gt(<?= $type['level'] - 1; ?>)').remove();
        $('div.option-cloumn-grid').append(result);
    }
});
$('.help-popup').popup({
    inline   : true,
    hoverable: true,
    position : 'top right',
    delay: {
      show: 300,
      hide: 800
    }
});
</script>
<?php endif; ?>