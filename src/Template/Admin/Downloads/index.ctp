<div class="ui error message" style="display:none;"></div>
<div class="ui info message" style="display:none;"></div>

<div class="ui bottom attached active tab segmen downloads" data-tab="first">
	<blockquote class="ui info message">
		<p>In order to edit the option, please delete the old option, then create new one.</p>
		<p>Options are ordered in alphabet.</p>
        <h5>CSV file format example:</h5>
        <code>
            Galaxy S4,Galaxy S5,Galaxy S6,Galaxy S7
        </code>
        <p><i class="icon circle warning red"></i>Do not put new line/return characters in the file, values can only be seperated by "," only.</p>
	</blockquote>
    <div class="ui three column grid option-cloumn-grid">
    	<?php foreach($types as $idx=>$type): ?>
    	<div class="column menu-options">
    		<h4><?= __($type['name']); ?> 
                <select class="dynamic-menu" name="type-<?= __($type['id']); ?>" cmdid="<?= __($type['id']); ?>" cmdlevel="<?= __($type['level']); ?>">
                    <?php foreach($type['options'] as $option): ?>  
                    <option class="item" value="<?= $option['id']; ?>">
                        <?= $option['name']; ?>
                    </option>
                    <?php endforeach; ?>
                </select>
                <span class="ui icon button tiny green" cmd="add-option" cmdid="<?= $type['id']; ?>"><i class="plus icon"></i></span>
            </h4>
    		<div id="MenuOption<?= $type['id']; ?>" class="ui vertical menu" style="width: 95%;">
    		<?php if(isset($type['options'])): ?>
    			<?php foreach($type['options'] as $option): ?>	
    			<div class="item">
    				<?= $option['name']; ?>
    				<span class="ui red tiny button label" cmd="delete-option" cmdid="<?= $option['id']; ?>" style="width:30px;"><i class="remove icon"></i></span>
    			</div>
    			<?php endforeach; ?>
    		<?php endif; ?>
    		</div>
    	</div>	
    	<?php endforeach; ?>
    </div>
</div>

<div id="AddOptionTemp" style="display:none;">
	<div class="item option-editor">
		<div class="ui transparent icon input">
			<input name="option-name" type="text" placeholder="New option name...">
			<div class="ui icon buttons">
				<span class="ui tiny button" cmd="save-option" parentid=""><i class="check icon"></i></span>
				<span class="ui tiny button" cmd="cancel-option" parentid=""><i class="remove icon"></i></span>
			</div>
		</div>
	</div>
</div>

<div class="ui modal import-modal small">
  <i class="close icon"></i>
  <div class="header"><i class="save icon"></i> Import bulk records</div>
  <div class="content">
    <blockquote>
        <p>You are about to import the bulk records into <span id="BulkImportTxt" class="ui label blue"></span>.</p>
        <p><i class="icon orange warning circle"></i>The original records will be wiped out, then import the new records.</p>
    </blockquote>
    <div class="field">
        <label class="ui label">CSV file: <input type="file" name="csv"/></label>
    </div>
  </div>
  <div class="actions">
    <input type="hidden" id="ParentId"/>
    <input type="hidden" id="TypeId"/>
    <div class="ui positive button" id="ImportBtn">Import</div>
  </div>
</div>

<script type="text/javascript">
$('<script>').appendTo('div.downloads').attr('src', '/js/Admin/downloads/index.js');
</script>