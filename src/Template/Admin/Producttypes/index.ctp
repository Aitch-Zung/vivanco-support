<div class="ui error message" style="display:none;"></div>
<div class="ui info message" style="display:none;"></div>
<div class="ui form producttypes">
	<h4 class="ui dividing header"><?= __('Editing area'); ?>&nbsp;&nbsp;<span class="modemsg"><?= __('[Add]'); ?></span></h4>
	<div class="four fields">
		<input type="hidden" id="targetid" value="" />
		<div class="field"><input type="text" name="name" placeholder="<?= __('Product type name here....'); ?>" maxlength="40" /></div>
		<div class="field"><label>Display:<select name="display"><option value="0"><?= __('No') ; ?></option><option value="1"><?= __('Yes') ; ?></option></select></label></div>
		<div class="field"><label>Image:<input type="file" name="file" /></label></div>
		<div class="field"><div class="ui button save-btn" cmd="edit"><i class="check blue icon"></i><?= __('Save'); ?></div></div>
	</div>
</div>

<table class="ui striped table producttypes">
	<tr>
		<th class="center aligned one wide"><?= __('ID'); ?></th>
		<th class="center aligned one wide"><?= __('Display'); ?></th>
		<th class="four wide"><?= __('Name'); ?></th>
		<th class="center aligned two wide"><?= __('Image'); ?></th>
		<th class="center aligned three wide"><?= __('Date'); ?></th>
		<th class="center aligned two wide"><?= __('Action'); ?></th>
	</tr>
	<?php foreach ($producttypes as $key => $type): ?>
	<tr>
		<td class="center aligned"><?= $key + 1; ?></td>
		<td class="center aligned"><?= $type->display ? '<i class="checkmark icon teal"></i>' : '<i class="remove icon red"></i>'; ?></td>
		<td><?= $type->name; ?></td>
		<td class="center aligned"><?= $type->img === '' ? '' : $this->Html->image($type->img, array('class'=>'ui image tiny')); ?></td>
		<td class="center aligned"><?= $type->modified; ?></td>
		<td class="center aligned">
			<div class="ui icon buttons">
			  <div class="ui button action" cmd="edit"   cmdid="<?= $type['id'] ?>"><i class="write green icon"></i></div>
			  <div class="ui button action" cmd="toggle" cmdid="<?= $type['id'] ?>" togglevalue="<?= $type['display']; ?>"><i class="toggle <?= $type->display ? 'on' : 'off'; ?> blue icon"></i></div>
			  <div class="ui button action" cmd="remove" cmdid="<?= $type['id'] ?>"><i class="remove circle red icon"></i></div>
			</div>
		</td>
		<input type="hidden" id="record-<?= $type['id'] ?>" value='<?= json_encode($type); ?>'/>
	</tr>
	<?php endforeach; ?>
	<input type="hidden" id="lastkey" value='<?= isset($key) ? $key : 0; ?>'/>
</table>
<script type="text/javascript">
$('<script>').appendTo('table.producttypes').attr('src', '/js/Admin/producttypes/index.js');
</script>