<div class="ui error message" style="display:none;"></div>
<div class="ui info message" style="display:none;"></div>
<div class="ui form products" id="EditProductForm">
	<h4 class="ui dividing header"><?= __('Editing area'); ?>&nbsp;&nbsp;<span class="modemsg"><?= __('[Add]'); ?></span></h4>
	<div class="four fields">
		<input type="hidden" id="targetid" value="" />
		<div class="field">
			<label>Type:<select name="type_id">
			<?php foreach ($producttypes as $key => $type) : ?>
				<option value="<?= $type->id ?>"><?= $type->name; ?></option>
			<?php endforeach; ?>		
			</select></label>
		</div>
		<div class="field"><input type="text" name="name" placeholder="<?= __('Product module here....'); ?>" maxlength="40" /></div>
		<div class="field"><label>Display:<select name="display"><option value="0"><?= __('No') ; ?></option><option value="1"><?= __('Yes') ; ?></option></select></label></div>
		<div class="field"><label>Image:<input id="MainPhoto" type="file" name="file" /></label></div>
	</div>
	<div class="four fields">
		<div class="field"><input type="text" name="color" placeholder="<?= __('Product color here....'); ?>" maxlength="30" /></div>
		<div class="field"><input type="text" name="edp" placeholder="<?= __('Product EDP here....'); ?>" maxlength="30" /></div>
		<div class="field"><input type="text" name="description" placeholder="<?= __('Product description here....'); ?>" maxlength="255" /></div>
		<div class="field">
			<div class="ui button save-btn" cmd="edit"><i class="check blue icon"></i><?= __('Save'); ?></div>
		</div>
	</div>
	<div class="four fields">
		<div class="field">
			<?= __('Gallery Images:'); ?>
		</div>
		<div class="field"><label>Img1:<input id="gfile1" type="file" name="gfile" /></label></div>
		<div class="field"><label>Img2:<input id="gfile2" type="file" name="gfile" /></label></div>
		<div class="field"><label>Img3:<input id="gfile3" type="file" name="gfile" /></label></div>
	</div>
	<div class="fields">
		<label><?= __('Details') ?> [Auto Typography] </label>
		<textarea id="details" name="details"></textarea>
	</div>
</div>

<table class="ui striped table products">
	<tr>
		<th class="center aligned one wide"><?= __('ID'); ?></th>
		<th class="center aligned one wide"><?= __('Display'); ?></th>
		<th class="center aligned one wide"><?= __('Type'); ?></th>
		<th class="two wide"><?= __('Details'); ?></th>
		<th class="center aligned two wide"><?= __('Image'); ?> / <?= __('Gallery'); ?></th>
		<th class="center aligned three wide"><?= __('Date'); ?></th>
		<th class="center aligned three wide"><?= __('Action'); ?></th>
	</tr>
	<?php foreach ($products as $key => $type): ?>
	<tr>
		<td class="center aligned"><?= $key + 1; ?></td>
		<td class="center aligned"><?= $type->display ? '<i class="checkmark icon teal"></i>' : '<i class="remove icon red"></i>'; ?></td>
		<td><?= $type->Types['name']; ?></td>
		<td>
			<div class="ui bulleted list">
				<a class="item">Module:<?= $type->model_no; ?></a>
				<a class="item">EDP:<?= $type->edp; ?></a>
				<a class="item">Color:<?= $type->color; ?></a>
			</div>
		</td>
		<td class="center aligned">
			<div class="ui two column grid">
				<div class="column">
					<?= $type->img === '' ? '' : $this->Html->image($type->img, array('class'=>'ui image tiny')); ?>
				</div>
				<div class="column">
					<?= $type->gimg1 === '' ? '' : $this->Html->image($type->gimg1, array('class'=>'ui image tiny', 'style'=>'max-width:20px;display:inline-block;')).'<i class="icon remove red remove-img" cmd="'.$type->id.'" cmdid="1"></i>'; ?>
					<?= $type->gimg2 === '' ? '' : $this->Html->image($type->gimg2, array('class'=>'ui image tiny', 'style'=>'max-width:20px;display:inline-block;')).'<i class="icon remove red remove-img" cmd="'.$type->id.'" cmdid="2"></i>'; ?>
					<?= $type->gimg3 === '' ? '' : $this->Html->image($type->gimg3, array('class'=>'ui image tiny', 'style'=>'max-width:20px;display:inline-block;')).'<i class="icon remove red remove-img" cmd="'.$type->id.'" cmdid="3"></i>'; ?>
				</div>
			</div>
		</td>
		<td class="center aligned"><?= $type->modified; ?></td>
		<td class="center aligned">
			<div class="ui icon buttons">
			  <div class="ui button action" cmd="edit"   cmdid="<?= $type['id'] ?>"><i class="write green icon"></i></div>
			  <div class="ui button action" cmd="toggle" cmdid="<?= $type['id'] ?>" togglevalue="<?= $type['display']; ?>"><i class="toggle <?= $type->display ? 'on' : 'off'; ?> blue icon"></i></div>
			  <div class="ui button action" cmd="remove" cmdid="<?= $type['id'] ?>"><i class="remove circle red icon"></i></div>
			</div>
		</td>
		<textarea id="record-<?= $type['id'] ?>" style="display:none;"><?= json_encode($type); ?></textarea>
	</tr>
	<?php endforeach; ?>
	<input type="hidden" id="lastkey" value='<?= isset($key) ? $key : 0; ?>'/>
</table>

<script type="text/javascript">
$('<script>').appendTo('table.products').attr('src', '/js/Admin/products/index.js');
</script>