<div class="ui error message" style="display:none;"></div>
<div class="ui form files">
	<h4 class="ui dividing header"><?= __('Editing area'); ?>&nbsp;&nbsp;<span class="modemsg"><?= __('[Add]'); ?></span></h4>
	<div class="four fields">
		<input type="hidden" id="targetid" value="" />
		<div class="field">
			<div class="ui info icon message"><?= __('Relating this uploaded file to:'); ?></div>
		</div>
		<div class="field">
			<label><?= __('EDP'); ?>:</label>
	        <select name="edp">
	        	<option value=""><?= __('Skip relating to EDP'); ?></option>
	            <?php foreach ($products as $key => $prd): ?>
	            <option value="<?= $prd->edp; ?>"><?= $prd->edp; ?></option>
	            <?php endforeach; ?>
	        </select>
		</div>
		<div class="field">
			<label><?= __('Product Name') ?>:</label>
	        <select name="model_no">
	        	<option value=""><?= __('Skip relating to name'); ?></option>
	            <?php foreach ($products as $key => $prd): ?>
	            <option value="<?= $prd->model_no; ?>"><?= $prd->model_no; ?></option>
	            <?php endforeach; ?>
	        </select>
		</div>
		<div class="field">
    		<label>Order No: <input type="text" name="voucherno" placeholder="<?= __('If entered, then the file is related to this No.'); ?>" maxlength="50" /></label>
    	</div>
	</div>
	<div class="div four fields dynamic-fields row">
    	<?php foreach($types as $idx=>$type): ?>
    	<div class="field menu-options">
    		<label><?= __($type['name']); ?> :</label>
    		<select id="MenuOption<?= $type['id']; ?>" name="extsearch<?= $idx + 1; ?>" class="dynamic-menu" cmdlevel="<?= $type['level']; ?>">
	    		<option value=""><?= __('Skip relating to {0}', [$type['name']]); ?></option>
	    		<?php if(isset($type['options'])): ?>
	    			<?php foreach($type['options'] as $option): ?>	
	    			<option value="<?= $option['name']; ?>" cmdid="<?= $option['id']; ?>">
	    				<?= $option['name']; ?>
	    			</option>
	    			<?php endforeach; ?>
	    		<?php endif; ?>
    		</select>
    	</div>	
    	<?php endforeach; ?>
    </div>

	<div class="four fields" style="margin-top:10px;">
		<div class="field"><input type="text" name="name" placeholder="<?= __('[OPTIONAL] New file name....'); ?>" maxlength="40" /></div>
		<div class="field"><label>Avaiable:<select name="display"><option value="0"><?= __('No') ; ?></option><option value="1"><?= __('Yes') ; ?></option></select></label></div>
		<div class="field"><label>File:<input type="file" name="file" /></label></div>
		<div class="field">
			<div class="ui button save-btn" cmd="edit"><i class="check blue icon"></i><?= __('Save'); ?></div>
			<div id="HelpBtn" class="ui circular blue icon button help-popup" data-content="<?= __('Need help?'); ?>" data-variation="inverted"><i class="help icon"></i></div>
		</div>
	</div>
</div>

<table class="ui striped table files">
	<tr>
		<th class="center aligned one wide">   <?= __('ID'); ?></th>
		<th class="center aligned one wide">   <?= __('Display'); ?></th>
		<th class="center aligned three wide"> <?= __('Name'); ?></th>
		<th class="center aligned five wide">  <?= __('Related'); ?></th>
		<th class="center aligned one wide">   <?= __('Download'); ?></th>
		<th class="center aligned three wide"> <?= __('Date'); ?></th>
		<th class="center aligned four wide">  <?= __('Action'); ?></th>
	</tr>
	<?php foreach($files as $key=>$file): ?>
	<tr>
		<?php 
			$relates = [ $file->edp, $file->model_no, $file->extsearch1, $file->extsearch2, $file->extsearch3, $file->voucherno !== '' ? 'Ord:'.$file->voucherno : '']; 
			$relates = array_filter($relates, function($a){ if(strlen($a)){ return $a; } });
		?>
		<td class="center aligned"><?= $key + 1; ?></td>
		<td class="center aligned"><?= $file->display ? '<i class="checkmark icon teal"></i>' : '<i class="remove icon red"></i>'; ?></td>
		<td><?= $file->name; ?></td>
		<td class="center aligned">
			<?= implode(', ', $relates); ?>
			<span id="VoucherNo<?= $file->id ?>" class="ui label blue info" style="display:none"></span>
		</td>
		<td class="center aligned"><?= $this->Html->link('<i class="download icon"></i>', $file->path, array('escape'=>false, 'target'=>'_blank')); ?></td>
		<td class="center aligned"><?= $file->modified; ?></td>
		<td class="center aligned">
			<div class="ui icon buttons">
			  <div class="ui button action" cmd="edit"   style="width: 35px;" cmdid="<?= $file['id'] ?>"><i class="write green icon"></i></div>
			  <div class="ui button action" cmd="toggle" style="width: 35px;" cmdid="<?= $file['id'] ?>" togglevalue="<?= $file['display']; ?>"><i class="toggle <?= $file->display ? 'on' : 'off'; ?> blue icon"></i></div>
			  <div class="ui button action" cmd="remove"  style="width: 35px;" cmdid="<?= $file['id'] ?>"><i class="remove circle red icon"></i></div>
			</div>
			<div class="ui icon buttons">
				<div class="ui button action help-popup" data-content="<?= __('Generate the Voucher Codes'); ?>" data-variation="inverted" cmd="get-vno" style="width: 35px;" cmdid="<?= $file['id'] ?>"><i class="barcode pink icon"></i></div>
			  	<div class="ui button action help-popup" data-content="<?= __('Show the Voucher Code CSV file'); ?>" data-variation="inverted" cmd="get-all-vno" style="width: 35px;" cmdid="<?= $file['id']; ?>"><i class="browser purple icon"></i></div>
			</div>
		</td>
		<input type="hidden" id="record-<?= $file['id'] ?>" value='<?= json_encode($file); ?>'/>
	</tr>
	<?php endforeach; ?>
</table>

<div class="ui modal help-modal">
  <i class="close icon"></i>
  <div class="header"><i class="icon help circle"></i> Help</div>
  <div class="content">
    <blockquote class="segment">
    	<h3>Upload the file</h3>
    	<ul>
    		<li>Press the <b>File</b> button to choose the file for uploading.</li>
    		<li>Specify the criterion for the file. The file can only be found under the specified criterion.</li>
    	</ul>
    	<h4>Examples</h4>
    	<ol>
	    	<li><code>
	    		If you chose the criterion like: "Smartphone" > "Apple" > "iPhone 6". Then files can only be downloaded by the users who have the Voucher Codes relate to these crterion.
	    	</code></li>
	    	<li><code>
	    		If you chose the criterion like: "Smartphone" > "Samsung". Then files can only be downloaded by the users who have the Voucher Codes relate to these crterion.
	    	</code></li>
    	</ol>
    </blockquote>
    <blockquote class="segment">
    	<h3>Generate the Voucher Codes</h3>
    	<ul>
    		<li>Upload the file first.</li>
    		<li>On the right side of each record, there 5 buttons. Click the pink button <i class="barcode pink icon"></i> to generate the Voucher Codes. </li>
    		<li>In order to get the generated Voucher Code list file, click the purple button <i class="browser purple icon"></i>. The link of the file will be shown below the <b>Related</b> field.</li>
    	</ul>
    </blockquote>
  </div>
</div>

<script type="text/javascript">
$('<script>').appendTo('table.files').attr('src', '/js/Admin/files/index.js');
</script>