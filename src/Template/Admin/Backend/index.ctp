<div class="three wide column s-menu">
	<div class="ui vertical menu">
	  <a class="item" cmd="producttypes"> <i class="certificate icon"></i>  <?= __('Product Types'); ?></a>
	  <a class="item" cmd="products">     <i class="cubes icon"></i>        <?= __('Products'); ?></a>
	  <a class="item" cmd="files">        <i class="cloud upload icon"></i> <?= __('Files'); ?></a>
	  <a class="item" cmd="downloads">    <i class="options icon"></i>      <?= __('Combo Settings'); ?></a>
	  <a class="item" cmd="settings">     <i class="settings icon"></i>     <?= __('Settings'); ?></a>
	  <a class="item" cmd="logout">       <i class="sign out icon"></i>     <?= __('Logout'); ?></a>
	</div>
</div>

<div class="twelve wide column d-content form">
	<h5><?= __('Welcome to the backend site'); ?></h5>
	<ul>
		<li><a class="item" cmd="producttypes"> <?= __('Product Types'); ?></a> : List the product types. </li>
		<li><a class="item" cmd="products">     <?= __('Products'); ?></a>      : List the products. </li>
		<li><a class="item" cmd="files">        <?= __('Files'); ?></a>         : Upload the files for downloads.</li>
		<li><a class="item" cmd="downloads">    <?= __('Combo Settings'); ?></a>: Manage the Combobox settings.</li>
		<li><a class="item" cmd="settings">     <?= __('Settings'); ?></a>      : General settings of the website.</li>
		<li><a class="item" cmd="logout">       <?= __('Logout'); ?></a>        : A way out.</li>
	</ul>
</div>

<script type="text/javascript">
	$(document).on('geh-zur-arbeit', function(){
		$('a.item').on('click', function($event){
			var  cmd = $(this).attr('cmd'),
			     form = $('div.form.d-content');

			if($(this).hasClass('active teal')){
				return;
			}

			form.addClass('loading');
			$('a.item').removeClass('active teal');
			$('a[cmd='+cmd+']').addClass('active teal');

			switch(cmd){
				case 'producttypes':	
				case 'products':	
				case 'files':	
				case 'downloads':
				case 'settings':	
					form.empty();
					form.load('/admin/'+cmd, {f:(new Date()).getTime()}, function(){
						form.removeClass('loading');
						//$('<script/>').appendTo(form).attr('src', '/js/Admin/'+cmd+'/index.js');
					});
					break;
				case 'logout' : 
					$.get('/admin/users/logout', function(result){
						location.href = result.redirect;
					}, 'json');
				break;
			}

			$event.preventDefault();
		});
	});
</script>