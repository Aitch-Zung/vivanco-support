<div class="ui form centered six wide column">
  <div class="ui ignored info message">
  	<blockquote><?= __('In order to login into the backend site, please enter your username and password below.'); ?></blockquote>
  </div>
  <div class="ui error message"></div>
  <div class="field input-field">
	  <label><?= __('Username') ?></label>
	  <input id="Username" placeholder="<?= __('valid username here...') ?>" type="text" maxlength="30" focus>
  </div>
  <div class="field input-field">
	  <label><?= __('Password') ?></label>
	  <input id="Password" placeholder="<?= __('valid password here...') ?>" type="password" maxlength="40">
  </div>
  <div class="field">
  	<div id="SubmitBtn" class="ui submit button"><?= __('Login'); ?></div>
  	<div class="ui pointing left label"><?= __('Prove you\'re not a robot, press Login btn.'); ?></div>
  </div>
</div>

<script type="text/javascript">
	$(document).on('geh-zur-arbeit', function(){
		var $username = $('#Username'),
		    $password = $('#Password');

		$('#Username').focus();

		$('#SubmitBtn').on('click', function($event){
			$('div.error.message').hide();
			$('div.input-field').removeClass('error');

			if($username.val() === '' || $password.val() === ''){
				$('div.error.message').html('<?= __('Please enter the username and password!'); ?>').show();
				$('div.input-field').addClass('error');
				return false;
			}else{
				$('div.form').addClass('loading');

				//verify the user
				$.post('/admin/users/login', { username: $username.val(), password: $password.val() },function(result){
					if(result.result){
						location.href = result.redirect;
					}else{
						$('div.form').removeClass('loading');
						$('div.error.message').html(result.message).show();
					}
				}, 'json');
			}

			$event.preventDefault();
		});
	});
</script>