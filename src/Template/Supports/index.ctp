<div class="ui bottom attached tab segment active" data-tab="Support">
    <h2>How to install the Vivanco Screen Protector:</h2>
    <li style="margin-left: 10em; float: left; width: 100%"><?= $this->Html->image('pic2-1.jpg', array('class'=>'ui medium image floated left', 'style'=>'width:220px;')); ?><?= __('Clean the display using the microfiber cloth and the dust remover'); ?></li>
    <li style="margin-left: 10em; float: left; width: 100%"><?= $this->Html->image('pic2-2.jpg', array('class'=>'ui medium image floated left', 'style'=>'width:220px;')); ?><?= __('Remove film no. 1 from the display protection'); ?></li>
    <li style="margin-left: 10em; float: left; width: 100%"><?= $this->Html->image('pic2-3.jpg', array('class'=>'ui medium image floated left', 'style'=>'width:220px;')); ?><?= __('Carefully attach this side, starting with the corners at the top of the device'); ?></li>
    <li style="margin-left: 10em; float: left; width: 100%"><?= $this->Html->image('pic2-4.jpg', array('class'=>'ui medium image floated left', 'style'=>'width:220px;')); ?><?= __('Remove possible air bubbles with the applicator'); ?></li>
    <li style="margin-left: 10em; float: left; width: 100%"><?= $this->Html->image('pic2-5.jpg', array('class'=>'ui medium image floated left', 'style'=>'width:220px;')); ?><?= __('Remove film no. 2 carefully'); ?></li>

    <li style="margin-left: 10em; float: left; width: 100%"><?= __('To download the full installation guide of display foils, please click '); ?><a href="/files/BDA-Screenprotector-A3.pdf" target="_blank">>>here<<</a></li>
</div>