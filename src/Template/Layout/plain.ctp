<?php
/**
 * Sematic-UI enabled layout
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 *
 */
?>
<?= $this->fetch('content') ?>