<?php
/**
 * Sematic-UI enabled layout
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 *
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$cakeDescription = Configure::read('WebSite.description');

if (isset($title)) {
    $this->assign('title', $title); 
}
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= Configure::read('WebSite.title');  ?>:
        <?= $this->fetch('title'); ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->script(array('jquery.min.js')); ?>
    <?= $this->Html->css(array('/semantic-ui/semantic.min.css', '/font/styles.css', 'base.css')) ?>
</head>
<body>
    <div id="wrapper">
        <header id="header">
            <div>
                <a href="/" class="icon icon-vivanco-logo1"></a>
                <h3><?= __('Vivanco Screen Protectors.') ; ?></h3>
            </div>
        </header>

        <!-- Dynamic content -->
        <div id="content" class="ui grid admin-content segment">
            <?= $this->Flash->render() ?>

            <?= $this->fetch('content') ?>
        </div>

        <footer id="footer">
            <div id="footer_info">
                <p class="copyright"><?= __('Copyright &copy; 2015 Vivanco.'); ?></p>
            </div>
        </footer>
    </div>

    <input type="hidden" id="path"       value="<?= '/'.$controllerName.'/'.$actionName; ?>"/>
    <input type="hidden" id="controller" value="<?= $controllerName; ?>"/>
    <input type="hidden" id="action"     value="<?= $actionName; ?>"/>

    <?= $this->Html->script(array('/semantic-ui/semantic.min.js', 'base.js')); ?>
</body>

</html>
