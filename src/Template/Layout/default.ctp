<?php
/**
 * Sematic-UI enabled layout
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 *
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$cakeDescription = Configure::read('WebSite.description');

if (isset($title)) {
    $this->assign('title', $title); 
}
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= Configure::read('WebSite.title');  ?>:
        <?= $this->fetch('title'); ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(array('/semantic-ui/semantic.min.css', '/font/styles.css', 'magnific-popup.css', 'base.css')) ?>
    <?= $this->Html->script(array('jquery.min.js')); ?>
</head>
<body>
    <div id="wrapper">
        <header id="header">
            <div>
                <a href="/" class="icon icon-vivanco-logo1"></a>
                <h3><?= __('Vivanco Screen Protectors') ; ?></h3>
            </div>
        </header>

        <div id="container">
            <!-- Main menu -->
            <div class="ui secondary pointing menu">
                <a class="item tab-menu" data-tab="Products" id="ProductLink" href="/products">
                    <i class="browser icon"></i> <?= __('Product'); ?>
                </a>
                <a class="item tab-menu" data-tab="Downloads" href="/downloads">
                    <i class="download icon"></i> <?= __('Downloads'); ?>
                </a>
                <a class="item tab-menu" data-tab="Supports" href="/supports">
                    <i class="configure icon"></i> <?= __('Support'); ?>
                </a>
                <div class="right menu">
                    <form class="item" action="/downloads/" method="post" id="SearchToolbarForm">
                        <div class="ui transparent icon input">
                            <input type="text" name="search" placeholder="<?= __('Search...'); ?>">
                            <i class="search link icon" onclick="$('#SearchToolbarForm').submit();"></i>
                        </div>
                    </form>   
                </div>
            </div>

            <!-- Dynamic content -->
            <div id="content">
                <?= $this->Flash->render() ?>

                <div class="row">
                    <?= $this->fetch('content') ?>
                </div>
            </div>

            <!-- hr -->
            <div class="line"></div>

            <!-- footer introduction -->
            <div class="ui two column grid">
                <div class="column">
                    <div class="ui horizontal segment">
                        <h4><?= __('Vivanco Screen Protectors'); ?></h4>
                        <p><?= __('Fit with every smartphone, just follow the steps and use the received code to find the pattern of your device.'); ?></p>
                    </div>
                </div>
                <div class="column">
                    <div class="ui horizontal segment">
                        <h4><?= __('Intelligent Solutions'); ?></h4>
                        <p> <?= __('Vivanco is one of Europe\'s leading providers of equipment and connecting accessories. As multispecialist we distribute products of the segments Consumer Electronics, Information Technology and Telecommunications.'); ?></p>
                    </div>
                </div>
            </div>
        </div>

        <footer id="footer">
            <div id="footer_info">
                <p class="copyright"><a href="/pages/imprint"><?= __('Imprint'); ?></a>&nbsp;&copy;&nbsp;<?= __('{0} Vivanco.', [date('Y')]); ?></p>
                <div class="ui tiny images">
                    <?= $this->Html->image('german.png', array('class'=>'ui image lang-flag', 'cmd' => 'de_DE')); ?>
                    <?= $this->Html->image('british.png', array('class'=>'ui image lang-flag', 'cmd' => 'en')); ?>
                </div>
            </div>
        </footer>
    </div>

    <input type="hidden" id="path"       value="<?= '/'.$controllerName.'/'.$actionName; ?>"/>
    <input type="hidden" id="controller" value="<?= $controllerName; ?>"/>
    <input type="hidden" id="action"     value="<?= $actionName; ?>"/>

    <?= $this->Html->script(array('/semantic-ui/semantic.min.js', 'jquery.magnific-popup.min.js', 'base.js')); ?>
</body>

</html>
