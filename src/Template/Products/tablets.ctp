<div id="ProductTabSeg" class="ui bottom attached active tab segment" data-tab="Product">
   <div class="ui buttons product-join">
      <div class="ui button sub-tab-menu" cmd="/products/index/smartphones"><?= __('Smartphones'); ?></div>
      <div class="or"></div>
      <div class="ui blue button sub-tab-menu"><?= __('Tablets'); ?></div>
   </div>
   <div class="ui divider"></div>
   <table class="ui celled table">
      <tbody>
         <tr>
            <td>
               <?= $this->Html->image('pic10.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for Google Nexus, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
            <td>
               <?= $this->Html->image('pic11.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for Asus Pad Screen Protector, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
         </tr>
         <tr>
            <td>
               <?= $this->Html->image('pic12.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for iPad mini2, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
            <td>
               <?= $this->Html->image('pic13.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for HTC Pad Screen Protector, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
         </tr>
      </tbody>
   </table>
</div>