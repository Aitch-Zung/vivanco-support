<div class="ui bottom attached active tab segment" data-tab="Product">
   <div class="ui buttons product-join">
      <?php foreach($producttypes as $key => $type): ?>
         <div class="ui <?= $productTypeName === $type->name ? 'blue' : '' ?> button sub-tab-menu" cmd="<?= $productTypeName !== $type->name ? '/products/index/'.$type->name : '' ?>" ><?= __($type->name); ?></div>
         <?= $key % 2 === 0 ? '<div class="or"></div>' : ''; ?>   
      <?php endforeach; ?>
   </div>
   <div class="ui divider"></div>
   <table class="ui celled table">
      <tbody>
         <tr>
         <?php $recordCount = $products->count(); ?>
         <?php foreach ($products as $key => $prd): ?>
            <td <?= $key >= $recordCount - 1 ? 'style="border-bottom: none;"' : ''; ?>>
               <div class="ui five column grid">
                  <div class="four wide column" style="text-align:center;">
                     <?= $this->Html->image($prd->img, array('class'=>'ui small image', 'style'=>'float:none; margin:0px; display: inline-block;')); ?><br/>
                     <span><a href="/products/read/<?= $prd->id, '/', strtolower($prd->model_no); ?>"><?= __('Details'); ?></a></span>
                  </div>
                  <div class="twelve wide column">
                     <p><?= $prd->description; ?></p>
                     <p><?= $prd->model_no; ?></p>
                     <p>Color: <?= $prd->color; ?></p>
                     <p>EDP: <?= $prd->edp; ?></p>
                  </div>
               </div>
            </td>
         <?= $key % 2 === 1 ? '</tr><tr>' : ''; ?>   
         <?php endforeach; ?>
      </tbody>
   </table>
   <input type="hidden" value="<?= $recordCount; ?>"/>
</div>