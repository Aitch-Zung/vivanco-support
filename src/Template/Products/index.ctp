<div class="ui bottom attached active tab segment" data-tab="Product">
    <h2><?= __('What are you searching for?'); ?></h2>
    
    <?php foreach($producttypes as $key => $type): ?>
    <div class="ui very relaxed items">
        <div class="ui card">
            <?= $this->Html->link(
                    $this->Html->image($type->img),
                    array('controller' => 'Products', 'action' => 'index', $type->name),
                    array('escape' => false, 'class' => 'image')
                ); ?>
        <div class="content">
            <a class="header" href="#link"><?= __($type->name); ?></a>
        </div>
        </div>
    </div>        
    <?php endforeach; ?>
</div>