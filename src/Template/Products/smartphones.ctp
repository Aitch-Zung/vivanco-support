<div class="ui bottom attached active tab segment" data-tab="Product">
   <div class="ui buttons product-join">
      <div class="ui blue button sub-tab-menu"><?= __('Smartphones'); ?></div>
      <div class="or"></div>
      <div class="ui button sub-tab-menu" cmd="/products/index/tablets"><?= __('Tablets'); ?></div>
   </div>
   <div class="ui divider"></div>
   <table class="ui celled table">
      <tbody>
         <tr>
            <td>
               <?= $this->Html->image('pic9.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for Sony Z2, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
            <td>
               <?= $this->Html->image('pic8.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for HTC 816 Screen Protector, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
         </tr>
         <tr>
            <td>
               <?= $this->Html->image('pic7.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for Samsung Galaxy, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
            <td>
               <?= $this->Html->image('pic6.jpg', array('class'=>'ui small image')); ?>
               <p>Vianco Screen Protector + microfiber cloth + dust remove for iPhone 5S Screen Protector, 2pce.</p>
               <h5>SPVVGS6</h5>
               <p>color:transparent</p>
               <p>EDP:35458</p>
            </td>
         </tr>
      </tbody>
   </table>
</div>