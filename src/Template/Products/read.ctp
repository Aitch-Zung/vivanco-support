<div class="ui centered grid">
	<div class="sixteen wide column row">
	  <div class="three wide column image" style="text-align:center">
      	<?= $this->Html->image($product->img); ?>
      	<div class="gallery ui six column grid align centered">
      		<?= $product->gimg1 === '' ? '' : '<div class="column">'.$this->Html->image($product->gimg1, array('class'=>'ui image tiny', 'style'=>'max-width:20px; cursor:pointer;', 'href'=>$product->gimg1)).'</div>'; ?>
      		<?= $product->gimg2 === '' ? '' : '<div class="column">'.$this->Html->image($product->gimg2, array('class'=>'ui image tiny', 'style'=>'max-width:20px; cursor:pointer;', 'href'=>$product->gimg2)).'</div>'; ?>
      		<?= $product->gimg3 === '' ? '' : '<div class="column">'.$this->Html->image($product->gimg3, array('class'=>'ui image tiny', 'style'=>'max-width:20px; cursor:pointer;', 'href'=>$product->gimg3)).'</div>'; ?>
      	</div>
      </div>
      <div class="twelve wide column">
        <div class="meta">
        	<a class="header"><?= $product->model_no; ?></a>
        </div>
	    <div class="meta">
	      <span><?= __('Color').': &nbsp;'.$product->color; ?></span>
	    </div>
	    <div class="meta">
	    	<span><?= __('EDP').': &nbsp;'.$product->edp; ?></span>
	    </div>
	    <div class="meta">
	      <p><?= $product->description; ?></p>
	    </div>
	    <div class="meta">
	      <?= nl2br($product->details); ?>
	    </div>
	    <?php if(isset($files) && $files->count()): ?>
	      <h5 class="field"><?= __('Related files'); ?>:</h5>
	      <table class="ui striped table files field">
	         <tr>
	            <th class="center aligned one wide">   <?= __('ID'); ?></th>
	            <th class="center aligned one wide">   <?= __('Name'); ?></th>
	            <th class="center aligned one wide">   <?= __('Download'); ?></th>
	         </tr>
	         <?php foreach($files as $key=>$file): ?>
	         <tr>
	            <td class="center aligned"><?= $key + 1; ?></td>
	            <td><?= $file->name; ?></td>
	            <td class="center aligned"><?= $this->Html->link('<i class="download icon"></i>', $file->path, array('escape'=>false, 'target'=>'_blank')); ?></td>
	         </tr>
	         <?php endforeach; ?>
	      </table>
	    <?php endif; ?>
	    <div class="meta">
	    	[ <a href="" onclick="history.back();"><?= __('Back'); ?></a> ]
	    </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  $(document).on('geh-zur-arbeit', function(){
  	$('div.gallery').magnificPopup({
		delegate: 'img',
		type: 'image',
		gallery:{
			enabled:true
		}
	});
  });
  </script>