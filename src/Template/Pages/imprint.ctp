<div class="ui bottom attached active tab segment">
   <h2><?= __('Imprint') ?></h2>
   <table style="height: 300px; margin: 0 0 2em 15em; width: 600px;">
      <tbody>
         <tr>
            <td class="labelStd" style="width: 250px;">Company:</td>
            <td>Vivanco Gruppe AG</td>
         </tr>
         <tr>
            <td class="labelStd">Address:</td>
            <td>Ewige Weide 15 <br> 22926 Ahrensburg</td>
         </tr>
         <tr>
            <td class="labelStd">Phone:</td>
            <td>+49 4102 - 231 0</td>
         </tr>
         <tr>
            <td class="labelStd">Fax:</td>
            <td>+49 4102 - 231 160</td>
         </tr>
         <tr>
            <td class="labelStd">Trade register:</td>
            <td>Ahrensburg</td>
         </tr>
         <tr>
            <td class="labelStd">Trade register number:</td>
            <td>HRB 3913</td>
         </tr>
         <tr>
            <td class="labelStd">Tax number:</td>
            <td>30 292 08216</td>
         </tr>
         <tr>
            <td class="labelStd">Sales tax identification number:</td>
            <td>DE 189326917</td>
         </tr>
      </tbody>
   </table>
   <div id="spacerStd" style="margin: 0 10em 2em 15em;">
      <div class="subHeadline">Intellectual property rights &amp; copyright:</div>
      <div id="spacerStd2">All texts, images and diagrams, as well as design and layout are protected by copyright and are - unless marked otherwise - intellectual property of Vivanco Gruppe AG. These contents may not be used, be it privately or commercially, without the explicit consent of Vivanco Gruppe AG. For any requests, please contact the Vivanco Gruppe AG.</div>
      
      <div id="spacerStd" class="subHeadline">Disclaimer:</div>
      <div id="spacerStd2">
      <p>All content available on this website was examined carefully. Nevertheless, there can be no absolute warranty or guarantee for correctness, completeness or topicality of the information and an undisturbed access. Vivanco Gruppe AG therefore is not liable for damages related to these contents or the usage of these contents.</p><p> Links to external pages do not lie within the sphere of Vivanco Gruppe AG's responsibility and are not subject of any liability. The editor does not relate to the linked contents and holds no responsibility for the correctness, completeness or topicality of the contents of the linked pages.</p><p> At the time of creation of the link, no illegal or accusable contents were found. Should this be the case at any time, please inform us. The link will be deleted immediately after examination of the circumstances. In general, Vivanco Gruppe AG is not obligated to examine these links for conformity with civil and penal law.</p></div>
   </div>
</div>

<style type="text/css">
.labelStd, .subHeadline{
	font-weight: bold; font-size: 1.2em;
}
.subHeadline{
	margin-top: 20px;
}
</style>