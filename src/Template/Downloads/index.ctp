<div class="ui bottom attached tab segment active" data-tab="Downloads">
   <div class="ui form segment dynamic-fields">
      <h2><?= __('Find your device in {0} steps', array(4)); ?>:</h2>
      <div class="ui info icon message field">
         <i class="info circle icon"></i>
         <p><?= __('You have to enter the Voucher Code in order to download the file.'); ?></p>
      </div>
      
      <?php foreach($types as $idx=>$type): ?>
      <div class="field menu-options">
         <label><?= __($type['name']); ?> :</label>
         <select id="MenuOption<?= $type['id']; ?>" name="extsearch<?= $idx + 1; ?>" class="dynamic-menu" cmdlevel="<?= $type['level']; ?>">
            <option value=""><?= __('Please select one {0}', [strtolower($type['name'])]); ?></option>
            <?php if(isset($type['options'])): ?>
               <?php foreach($type['options'] as $option): ?>  
               <option value="<?= $option['name']; ?>" cmdid="<?= $option['id']; ?>">
                  <?= $option['name']; ?>
               </option>
               <?php endforeach; ?>
            <?php endif; ?>
         </select>
      </div>   
      <?php endforeach; ?>
      <div class="field">
         <label>Voucher Code: <input type="text" name="vouchernotxt" placeholder="<?= __('Mandatory'); ?>" maxlength="50" /></label>
      </div>

      <div class="field center aligned">
         <div id="SubmitBtn" class="ui blue submit button"><?= __('GO'); ?></div>
      </div>

      <?php if(isset($files) && count($files) > 0): ?>
      <h5 class="field"><?= __('Matching results'); ?>:</h5>
      <table class="ui striped table files field">
         <tr>
            <th class="center aligned one wide">   <?= __('ID'); ?></th>
            <th class="center aligned one wide">   <?= __('Name'); ?></th>
            <th class="center aligned five wide">  <?= __('Related'); ?></th>
            <th class="center aligned three wide"> <?= __('Date'); ?></th>
            <th class="center aligned one wide">   <?= __('Download'); ?></th>
         </tr>
         <?php foreach($files as $key=>$file): ?>
         <?php 
            $relates = [ $file->edp, $file->model_no, $file->extsearch1, $file->extsearch2, $file->extsearch3, $file->voucherno !== '' ? 'Order No:'.$file->voucherno : '']; 
            $relates = array_filter($relates, function($a){ if(strlen($a)){ return $a; } });
         ?>
         <tr>
            <td class="center aligned"><?= $key + 1; ?></td>
            <td><?= $file->name; ?></td>
            <td class="center aligned"><?= implode(', ', $relates); ?></td>
            <td class="center aligned"><?= $file->modified; ?></td>
            <td class="center aligned"><?= $this->Html->link('<i class="download icon"></i>', $file->path, array('escape'=>false, 'target'=>'_blank', 'class'=>'download-btn', 'cmd'=>$file->voucherno)); ?></td>
         </tr>
         <?php endforeach; ?>
      </table>
      <?php endif; ?>
      
      <form id="SearchForm" style="display:none;" method="post">
         <input type="hidden" name="edp"/>
         <input type="hidden" name="modelname"/>
         <input type="hidden" name="extsearch1"/>
         <input type="hidden" name="extsearch2"/>
         <input type="hidden" name="extsearch3"/>
         <input type="hidden" name="voucherno"/>
      </form>
      <input type="hidden" id="TargetVoucherNo" value="<?= isset($voucherno) ? $voucherno : ''; ?> "/>
   </div>
</div>

<script type="text/javascript">
$(document).on('geh-zur-arbeit', function(){
   
   $('#SubmitBtn').on('click', function($event){
      var edp     = $('#EDP').val(),
          modelno = $('#ModelNo').val();

      if(!edp && !modelno){
         //return false;
      }

      $(this).addClass('disabled loading');

      $('input[name=extsearch1]').val($('select[name=extsearch1]').val());
      $('input[name=extsearch2]').val($('select[name=extsearch2]').val());
      $('input[name=extsearch3]').val($('select[name=extsearch3]').val());
      $('input[name=voucherno]').val($('input[name=vouchernotxt]').val());

      $('#SearchForm').submit();
      $event.preventDefault();
   });

   $('a.download-btn').click(function($event){
      if(!confirm('<?= __('You can only download once, are you sure ?'); ?>')){
         $event.preventDefault();
         return false;
      }
      var cmd = $('#TargetVoucherNo').val();
      if(!cmd){
         $event.preventDefault();
         return false;  
      }
      $('#TargetVoucherNo').val('');
      $(this).find('i').removeClass('download').addClass('remove red').on('click', false);

      $.post('/downloads/toggle', { vno: cmd}, function(result){
      });
   });

   $('div.dynamic-fields').on('change', 'select.dynamic-menu', function($event){
      var value = $('option:selected', this).attr('cmdid'),
          level  = $(this).attr('cmdlevel') || 1,
          $selection = $(this);
      if(value){
         $.post('/downloads/getsub', {parentid:value }, function(result){
            console.info(result);
            if(result){
               $('div.dynamic-fields select.dynamic-menu:gt('+(level - 1)+')').parents('div.field.menu-options').remove();
               $selection.parents('div.field.menu-options').after(result);
               console.info($selection.parents('div.field.menu-options'));
            }
         });
      }
      $event.preventDefault();
   });
});
</script>
