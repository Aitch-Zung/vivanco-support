<?php //debug($types); ?>
<?php foreach($types as $idx=>$type): ?>
<div class="field menu-options">
    <label><?= __($type['name']); ?> :</label>
    <select id="MenuOption<?= $type['id']; ?>" name="extsearch<?= $type['level']; ?>" class="dynamic-menu" cmdlevel="<?= $type['level']; ?>">
        <option value=""><?= __('Please select one {0}', [strtolower($type['name'])]); ?></option>
        <?php if(isset($type['options'])): ?>
            <?php foreach($type['options'] as $option): ?>  
            <option value="<?= $option['name']; ?>" cmdid="<?= $option['id']; ?>">
                <?= $option['name']; ?>
            </option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>  
<?php endforeach; ?>