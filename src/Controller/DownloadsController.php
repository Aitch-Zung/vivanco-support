<?php 
/**
 * Download page controller
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 */
namespace App\Controller;

use \Cake\Core\Configure;
use \Cake\Network\Exception\NotFoundException;
use \Cake\View\Exception\MissingTemplateException;
use \Cake\ORM\TableRegistry as TableRegistry;

class DownloadsController extends AppController {

	public function index(){

		if($this->request->is('post')){
			$edp       = $this->request->data('edp');
			$modelname = $this->request->data('modelname');
			$search    = $this->request->data('search');

			$extsearch1 = $this->request->data('extsearch1');
			$extsearch2 = $this->request->data('extsearch2');
			$extsearch3 = $this->request->data('extsearch3');
			$voucherno  = $this->request->data('voucherno');

			$conditions = [];
			if($edp && !$search){
				$conditions['edp'] = $edp;
			}
			if($modelname && !$search){
				$conditions['model_no'] = $modelname;
			}

			if($search){
				$conditions['Files.name LIKE']       = '%'.$search.'%';	
				$conditions['Files.edp LIKE']        = '%'.$search.'%';	
				$conditions['Files.model_no LIKE']   = '%'.$search.'%';	
			}

			$andConditions = ['Files.display'=>1];

			if(count($conditions)){
				$whereParams['OR'] = $conditions;
			}else{
				if($extsearch1){
					$andConditions['extsearch1'] = $extsearch1;
				}
				if($extsearch2){
					$andConditions['extsearch2'] = $extsearch2;
				}
				if($extsearch3){
					$andConditions['extsearch3'] = $extsearch3;
				}
			}

			$whereParams = ['AND'=>$andConditions];	

			$files = [];
			if(!$voucherno){
				$files = TableRegistry::get('Files')->find('all')->where($whereParams)->order(['counter'=>'desc']);
			}else{
				if(TableRegistry::get('Downloadlogs')->find('all', [ 'conditions'=>['voucherno'=>$voucherno, 'is_used'=>0] ])->count()){
					$files = TableRegistry::get('Files')->find('all')->where($whereParams)->order(['counter'=>'desc']);
				}
				$this->set('voucherno', $voucherno);
			}

			$this->set('files', $files);
		}

		$types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['level'=>1]])->toArray();

        foreach($types as $idx=>$type){
            $options = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['type_id' => $type->id], 'order'=>['name'=>'asc']]);
            if($options){
                $types[$idx]['options'] = $options->toArray();
            }
        }
		
		$this->set(compact(array('types')));

	}//EOA

	public function toggle(){
		$this->autoRender = false;

		if($this->request->is('post')){
			$result = array(
                'result'  => false,
                'message' => ''
            );

			$vno = $this->request->data('vno');            
			$result['result'] = TableRegistry::get('Downloadlogs')->query()->update(['is_used'])->set(['is_used'=>1])->where(['voucherno'=>$vno])->execute();

			exit(json_encode($result));
		}
	}

	public function getsub($rendertype='usual'){
        
        if($this->request->is('post')){
            $this->autoRender = true;
            $this->layout     = 'plain';
            
            $parentid = $this->request->data('parentid');
            $menu = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['id'=>$parentid]])->first();
            $type = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['id'=>$menu['type_id']]])->first();

            $types = [];
            $submenus = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['parent_id' => $parentid], 'order'=>['name'=>'asc']]);

            if($submenus->count()){
                $submenus = $submenus->toArray();

                //Get type
                $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['id'=>$submenus[0]['type_id']]])->toArray();
                $types[0]['options'] = $submenus;
            }else{
                $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['level'=>($type['level'] + 1)]])->toArray();
            }

            $this->set(compact(['types', 'parentid']));
        }
    }

}//EOC

//EOF