<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class LanguagesController extends AppController {
	public function setLang($lang){
		$this->autoRender = false;

		$availableLanguages = Configure::read('AvailableLanguages');

		$targetLanguage = in_array($lang, $availableLanguages) ? $lang : Configure::read('DefaultLanguage');

		$session = $this->request->session();

		echo json_encode(array('result' => $session->write('TargetLanguage', $lang)));
	}
}