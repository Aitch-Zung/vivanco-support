<?php
namespace App\Controller\Admin;

class UsersController extends AppController {

	public function login(){
        if($this->request->is('post')){

            $this->autoRender = false;

            $username = addslashes($this->request->data('username'));
            $password = addslashes($this->request->data('password'));
            $filter = array(
                'Users.username' => $username,
                'Users.password' => sha1($password)
            );

            $result = $this->Users->find('all', array('conditions' => $filter));
            $loginReult = $result->first();

            if($loginReult === null){
                $message = __('Not a valid user');
            }else{
                $message = __('Valid user');
                $this->loadComponent('Auth');
                $this->Auth->setUser($loginReult->toArray());
            }

            exit(json_encode(
                array(
                    'result' => $loginReult ? true : false,
                    'message' => $message,
                    'redirect' => $loginReult ? '/admin/backend' : '' 
                )
            ));
        }
    }

    public function logout(){
        $this->autoRender = false;
        $this->loadComponent('Auth');
        $this->Auth->logout();

        exit('{"result":"true", "redirect":"/admin/users/login"}');
    }
}