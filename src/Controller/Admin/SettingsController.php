<?php 
/**
 * Download page controller
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 */
namespace App\Controller\Admin;

use \Cake\Event\Event as Event;
use \Cake\ORM\TableRegistry as TableRegistry;
use \Cake\Core\Configure as Configure;

class SettingsController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->layout = 'plain';
    }

	public function index(){

	}//EOA

    public function modpass(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $result = array(
                'result'  => false,
                'message' => ''
            );

            $newPassword = $this->request->data('newpassword');
            $oldPassword = $this->request->data('oldpassword');

            if($newPassword === '' || $oldPassword === ''){
                $result['message'] = __('Empty password strings');
                exit(json_encode($result));
            }

            $user = TableRegistry::get('Users')->find('all', ['conditions'=>['Users.password'=>sha1($oldPassword)]]);
            if(!$user->first()){
                $result['message'] = __('Old password is not correct');
            }else{
                $record = TableRegistry::get('Users')->query();
                $result['result'] = $record->update(['password'])->set(['password' => sha1($newPassword)])->execute();
            }

            exit(json_encode($result));
        }
    }

}//EOC

//EOF