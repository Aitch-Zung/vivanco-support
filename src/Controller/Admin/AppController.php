<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use \Cake\Controller\Controller as Controller;
use \Cake\Event\Event as Event;
use \Cake\Core\Configure as Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize(){
        $this->loadComponent('Flash');
    }

    public function beforeRender(Event $event){
        parent::beforeRender($event);

        $controllerName = $this->request->param('controller');
        $actionName = $this->request->param('action');

        $this->layout = 'admin';

        $this->set(compact('controllerName', 'actionName'));
    }

    public function uploadToLocalServer($imgLocation, $newFileName, $rootPath='') {
        $rootPath = $rootPath === '' ? Configure::read('UPD_FOLDER') : $rootPath;
        return move_uploaded_file($imgLocation, $rootPath . $newFileName);
    }

    public function shrinkImg($imgLocation, $width = 800, $height = 800) {
        //echo $imgLocation;

        $imagick = new \Imagick();

        list($width_orig, $height_orig) = getimagesize($imgLocation);

        if ($width_orig < $width) {
            return;
        }

        $ratio_orig = $width_orig / $height_orig;

        if ($width / $height > $ratio_orig) {
            $width = $height * $ratio_orig;
        } else {
            $height = $width / $ratio_orig;
        }

        $imagick->readImage($imgLocation);
        $imagick->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1);

        $imagick->writeImage($imgLocation);
        $imagick->clear();
        $imagick->destroy();
    }

    public function uploadImgs($uid = null) {

        if ($uid === null) {
            return;
        }

        $this->layout = 'simple';

        if (isset($this->params['form']['imgs'])) {
            $validUploadeds = array();
            $invalidUploadeds = array();
            $failedUploadings = array();
            $successUploadings = array();
            $phpIniUploadMaxSizeString = ini_get('upload_max_filesize');
            $validImgExts = array('pjpeg', 'xpng', 'jpeg', 'jpg', 'png', 'gif');
            $uploadMaxSize = Configure::read('MAX_UPD_SIZE');
            $uploadedImgs = $this->params['form']['imgs'];

            foreach ($uploadedImgs['name'] as $idx => $img) {
                if ($img != '') {
                    if (intval($uploadedImgs['size'][$idx]) > intval($uploadMaxSize)) {
                        $invalidUploadeds[] = $img . __('Over the size limit: {0}', [$uploadMaxSize]);
                    } else if (!$this->Utils->verifyFileExtension($img, $validImgExts)) {
                        $invalidUploadeds[] = __('Wrong image type: {0}', [$img]);
                    } else {
                        $imgFile = array(
                            'name' => $img,
                            'tmp_name' => $uploadedImgs['tmp_name'][$idx]
                        );
                        $validUploadeds[] = $imgFile;
                    }
                }
            }

            if (count($validUploadeds) > 0) {
                $newFileName = '';
                //move images and resize them
                foreach ($validUploadeds as $idx => $img) {
                    $ext = $this->getExt($img['name']);
                    $newFileName = 'U' . $uid . '_' . sha1($img['name']) . '_' . date('YmdHis') . '.' . $ext;

                    try {
                        $this->shrinkImg($img['tmp_name'], 650, 800);

                        $response = $this->uploadToLocalServer($img['tmp_name'], $newFileName);
                        if ($response) {
                            $successUploadings[] = array(
                                'orgName' => __('{0} uploaded', [$img['name']]),
                                'newName' => Configure::read('IMG_ROOT') . $newFileName
                            );
                        } else {
                            $failedUploadings[] = __('{0} cannot be uploaded.', [$img['name']]);
                        }
                    } catch (Exception $e) {
                        exit();
                    }
                }
            }//eo if()

            $this->set(compact('invalidUploadeds', 'failedUploadings', 'successUploadings'));
        }//eo if()

        $this->set('uid', $uid);
    }

    public function getExt($fileName){
        $fileNameParts = explode('.',$fileName);
        $ext = strtolower(array_pop($fileNameParts));
        return $ext;
    }
}
