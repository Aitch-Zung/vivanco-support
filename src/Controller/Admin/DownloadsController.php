<?php
namespace App\Controller\Admin;

use \Cake\Event\Event as Event;
use \Cake\ORM\TableRegistry as TableRegistry;
use \Cake\Core\Configure as Configure;

class DownloadsController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->layout = 'plain';
    }

    public function index(){
        //load download types:
        $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['level'=>1]])->toArray();

        foreach($types as $idx=>$type){
            $options = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['type_id' => $type->id], 'order'=>['name'=>'asc']]);
            if($options){
                $types[$idx]['options'] = $options->toArray();
            }
        }

        $this->set('types', $types);
	}

    public function getsub($rendertype='usual'){
        
        if($this->request->is('post')){
            $this->autoRender = true;
            $this->layout     = 'plain';
            
            $parentid = $this->request->data('parentid');
            $menu = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['id'=>$parentid]])->first();
            $type = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['id'=>$menu['type_id']]])->first();

            $types = [];
            $submenus = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['parent_id' => $parentid], 'order'=>['name'=>'asc']]);

            if($submenus->count()){
                $submenus = $submenus->toArray();

                //Get type
                $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['id'=>$submenus[0]['type_id']]])->toArray();
                $types[0]['options'] = $submenus;
            }else{
                $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['level'=>($type['level'] + 1)]])->toArray();
            }

            $this->set(compact(['types', 'parentid']));

            if($rendertype === 'plain'){
                $this->render('/Admin/Downloads/getpsub');
            }
        }
    }

    public function addoption(){
        $this->autoRender = false;
        
        if($this->request->is('post')){
            $id       = $this->request->data('typeid');
            $name     = $this->request->data('name');
            $parentid = $this->request->data('parentid');

            $result = array(
                'result'  => false,
                'message' => ''
            );

            $tbl = TableRegistry::get('Downloadoptions')->query();

            $columns = ['type_id', 'name', 'parent_id'];
            $values = [
                'type_id'   => $id,
                'name'      => $name,
                'parent_id' => $parentid
            ];
            $tbl->insert($columns)->values($values);

            if($tbl->execute()){
                $result['result'] = true;
                $result['message'] = __('Option saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('Option not saved');
            }

            exit(json_encode($result));
        }
    }

    public function deloption(){
        if($this->request->is('post')){
            $id = $this->request->data('id');

            $record = TableRegistry::get('Downloadoptions')->query();
            if($record->delete()->where(['id'=>$id])->execute()){
                $result['result'] = true;
                $result['message'] = __('Option deleted');
            }else{
                $result['result'] = false;
                $result['message'] = __('Option not deleted');
            }

            exit(json_encode($result));
        }
    }

    public function bulkimport(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

            $parentid = $this->request->data('parentid');
            $typeid   = $this->request->data('typeid');
            $csv      = $this->request->data('csv');

            $result = array(
                'result'  => false,
                'message' => ''
            );

            if(!isset($csv['tmp_name'])){
                $result['message'] = 'Invalid file uploaded';
                exit(json_encode($result));
            }

            if(!in_array($csv['type'], $mimes)){
                $result['message'] = 'Invalid file type';
                exit(json_encode($result));
            }

            $records = fgetcsv(fopen($csv['tmp_name'], 'r'));

            //clear the original records
            $tbl = TableRegistry::get('Downloadoptions');
            $tbl->query()->delete()->where(['parent_id'=>$parentid])->execute();

            $columns = ['type_id', 'name', 'parent_id'];
            $valueQuery = $tbl->query();

            foreach($records as $idx=>$rec){
                $name = trim($rec);
                if($name !== ''){
                    $values = [
                        'type_id'   => $typeid,
                        'name'      => $name,
                        'parent_id' => $parentid
                    ];
                    $valueQuery->insert($columns)->values($values);
                }
            }

            $exeResult = $valueQuery->execute();

            $result = array(
                'result'  => $exeResult,
                'message' => count($records) . ' records imported '.($exeResult ? 'successed' : 'failed')
            );

            exit(json_encode($result));
        }
    }
}