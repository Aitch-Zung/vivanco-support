<?php
namespace App\Controller\Admin;

use Cake\Event\Event as Event;

class BackendController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function beforeFilter(Event $event){
        //$this->Auth->allow(['index']);
    }
	
	public function index(){
		
	}
	
}