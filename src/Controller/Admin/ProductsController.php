<?php
namespace App\Controller\Admin;

use \Cake\Event\Event as Event;
use \Cake\ORM\TableRegistry as TableRegistry;
use \Cake\Core\Configure as Configure;

class ProductsController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function index(){
        $producttypes = TableRegistry::get('Producttypes')->find('all', ['conditions' => ['Producttypes.display'=>1]]);

        $products = $this->Products->find('all', ['order' => ['Products.type_id']])
                                   ->select(['Products.id', 'Products.model_no', 'Products.edp', 'Products.color', 'Products.display', 
                                             'Products.created', 'Products.modified', 'Products.type_id', 'Products.img', 'Products.description',
                                             'Products.gimg1', 'Products.gimg2', 'Products.gimg3', 
                                             'Products.details',
                                             'Types.name'])
                                   ->innerJoin(['Types'=>'producttypes'], ['Products.type_id=Types.id'])
                                   ->order(['Products.id'=>'desc']);

        $this->set(compact(array('producttypes', 'products')));
	}

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->layout = 'plain';
    }

    public function edit(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id      = $this->request->data('id');
            $name    = $this->request->data('name');
            $display = $this->request->data('display');
            $edp     = $this->request->data('edp');
            $desp    = $this->request->data('description');
            $details = addslashes($this->request->data('details'));
            $color   = $this->request->data('color');
            $typeid  = $this->request->data('typeid');
            $files   = $this->request->data('files');
            $gfile1  = $this->request->data('gfile1');
            $gfile2  = $this->request->data('gfile2');
            $gfile3  = $this->request->data('gfile3');

            $result = array(
                'result'  => false,
                'message' => ''
            );

            //$this->log('Files :'.print_r($files, true), 'debug');

            $img = '';
            $gimg1 = '';
            $gimg2 = '';
            $gimg3 = '';
            if(isset($files[0]) && isset($files[0]['tmp_name'])){
                $newFileName = sha1($files[0]['name']) . '_' . date('YmdHis') . '.' . $this->getExt($files[0]['name']);
                $this->shrinkImg($files[0]['tmp_name'], 442, 344);
                $response = $this->uploadToLocalServer($files[0]['tmp_name'], $newFileName);
                $img = Configure::read('IMG_ROOT') . $newFileName;
            }

            for($idx = 0 ; $idx <=2 ; $idx++){
                $param = 'gfile' . ($idx + 1);
                //$this->log($param.' :'.print_r($$param, true), 'debug');
                if(isset($$param) && count($$param)){
                    $var = 'gimg' . ($idx + 1);
                    //$this->log($param.' :'.print_r($$param, true), 'debug');
                    $targetFile = $$param;
                    //$this->log($param.' :'.$targetFile['name'], 'debug');
                    $newFileName = sha1($targetFile['name']) . '_' . date('YmdHis') . '.' . $this->getExt($targetFile['name']);
                    $this->shrinkImg($targetFile['tmp_name'], 442, 344);
                    $response = $this->uploadToLocalServer($targetFile['tmp_name'], $newFileName);
                    $$var = Configure::read('IMG_ROOT') . $newFileName;
                }
            }            

            /*for($idx = 0 ; $idx <=2 ; $idx++){
                if(isset($gfiles[$idx]) && isset($gfiles[$idx]['tmp_name'])){
                    $var = 'gimg' . ($idx + 1);
                    $newFileName = sha1($gfiles[$idx]['name']) . '_' . date('YmdHis') . '.' . $this->getExt($gfiles[$idx]['name']);
                    $this->shrinkImg($gfiles[$idx]['tmp_name'], 442, 344);
                    $response = $this->uploadToLocalServer($gfiles[$idx]['tmp_name'], $newFileName);
                    $$var = Configure::read('IMG_ROOT') . $newFileName;
                }
            }*/

            if(!$id){//CREATE
                $record = $this->Products->query();
                $columns = ['model_no', 'display', 'edp', 'description', 'details', 'color', 'type_id', 'img', 'gimg1', 'gimg2', 'gimg3'];
                $values = [
                    'type_id' => $typeid,
                    'model_no' => $name, 
                    'display' => $display, 
                    'edp' => $edp,
                    'description' => $desp,
                    'details' => $details,
                    'color' => $color,
                    'img' => $img,
                    'gimg1' => $gimg1,
                    'gimg2' => $gimg2,
                    'gimg3' => $gimg3
                ];

                $record->insert($columns)->values($values);
            }else{//UPDATE
                $columns = ['model_no', 'display', 'edp', 'description', 'details', 'color', 'type_id'];
                $values = [
                    'type_id' => $typeid,
                    'model_no' => $name, 
                    'display' => $display, 
                    'edp' => $edp,
                    'description' => $desp,
                    'details' => $details,
                    'color' => $color
                ];
                if($img !== ''){
                    $columns[] = 'img';
                    $values['img'] = $img;
                }
                if($gimg1 !== ''){
                    $columns[] = 'gimg1';
                    $values['gimg1'] = $gimg1;
                }
                if($gimg2 !== ''){
                    $columns[] = 'gimg2';
                    $values['gimg2'] = $gimg2;
                }
                if($gimg3 !== ''){
                    $columns[] = 'gimg3';
                    $values['gimg3'] = $gimg3;
                }

                $record = $this->Products->query();
                $record->update($columns)->set($values)->where(['id' => $id]);
            }

            if($record->execute()){
                $result['result'] = true;
                $result['message'] = __('Product saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product not saved');
            }

            exit(json_encode($result));
        }
    }

    public function toggle(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');
            $toggle = $this->request->data('toggle');

            $record = $this->Products->query();
            $record->update(['display'])->set(['display'=>$toggle])->where(['id' => $id]);

            if($record->execute()){
                $result['result'] = true;
                $result['message'] = __('Product type saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product type not saved');
            }

            exit(json_encode($result));
        }
    }

    public function delete(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');

            $record = $this->Products->query();
            if($record->delete()->where(['id'=>$id])->execute()){
                $result['result'] = true;
                $result['message'] = __('Product type deleted');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product type not deleted');
            }

            exit(json_encode($result));
        }        
    }

    public function removeimg(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');
            $idx = $this->request->data('idx');

            $record = $this->Products->query();
            $field = 'gimg' . $idx;

            $fields = [ $field ];
            $values = [];
            $values[$field] = '';

            if($record->update($fields)->set($values)->where(['id' => $id])->execute()){
                $result['result'] = true;
                $result['message'] = __('Product deleted');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product not deleted');
            }

            exit(json_encode($result));
        }           
    }
	
}