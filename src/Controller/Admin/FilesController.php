<?php 
/**
 * Download page controller
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 */
namespace App\Controller\Admin;

use \Cake\Event\Event as Event;
use \Cake\ORM\TableRegistry as TableRegistry;
use \Cake\Core\Configure as Configure;

class FilesController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->layout = 'plain';
    }

	public function index(){

		$files = $this->Files->find('all');

        $products = TableRegistry::get('Products')->find('all', ['conditions' => ['Products.display'=>1]])->order(['Products.type_id']);

        $types = TableRegistry::get('Downloadtypes')->find('all', ['conditions'=>['level'=>1]])->toArray();

        foreach($types as $idx=>$type){
            $options = TableRegistry::get('Downloadoptions')->find('all', ['conditions'=>['type_id' => $type->id], 'order'=>['name'=>'asc']]);
            if($options){
                $types[$idx]['options'] = $options->toArray();
            }
        }

        $this->set(compact(array('products', 'files', 'types')));

	}//EOA

	public function upload(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id      = $this->request->data('id');
            $name    = $this->request->data('name');
            $display = $this->request->data('display');
            $edp     = $this->request->data('edp');
            $modelno = $this->request->data('model_no');
            $files   = $this->request->data('files');

            $extsearch1 = $this->request->data('extsearch1');
            $extsearch2 = $this->request->data('extsearch2');
            $extsearch3 = $this->request->data('extsearch3');
            $voucherno  = $this->request->data('voucherno');

            $extsearch1 = $extsearch1 ? $extsearch1 : '';
            $extsearch2 = $extsearch2 ? $extsearch2 : '';
            $extsearch3 = $extsearch3 ? $extsearch3 : '';
            $voucherno  = $voucherno  ? $voucherno : '';

            $result = array(
                'result'  => false,
                'message' => ''
            );

            $newFileName = $name;
            $img = '';//image file, represent the uploaded file
            if(isset($files[0]) && isset($files[0]['tmp_name'])){
                $newFileName = $name ? $name .'.'. $this->getExt($files[0]['name']) : date('YmdHis') . $files[0]['name'];
                $response = $this->uploadToLocalServer($files[0]['tmp_name'], $newFileName, Configure::read('FILE_UPD_FOLDER'));
                $img = Configure::read('FLE_ROOT') . $newFileName;
            }else if(!$id){
            	//$result['message'] = 'Failed to build the file';
            	//exit(json_encode($result));
            }

            $record = $this->Files->query();
            $columns = ['model_no', 'display', 'edp', 'name', 'path', 'extsearch1', 'extsearch2', 'extsearch3', 'voucherno'];
            $values = [
            	'edp'        => $edp ? $edp : '',
                'model_no'   => $modelno ? $modelno : '', 
                'name'       => $newFileName,
                'display'    => $display,
                'voucherno'  => $voucherno,
                'extsearch1' => $extsearch1,
                'extsearch2' => $extsearch2,
                'extsearch3' => $extsearch3
            ];

            if(!$id){//CREATE
            	$values['path'] = $img;
                $record->insert($columns)->values($values);
            }else{//UPDATE
                if($img !== ''){
                    $columns[] = 'path';
                    $values['path'] = $img;
                }
                $record->update($columns)->set($values)->where(['id' => $id]);
            }

            if($record->execute()){
            	$result['result'] = true;
                $result['message'] = __('File saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('File not saved');
            }

            exit(json_encode($result));
        }
    }

    public function toggle(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');
            $toggle = $this->request->data('toggle');

            $record = $this->Files->query();
            $record->update(['display'])->set(['display'=>$toggle])->where(['id' => $id]);

            if($record->execute()){
                $result['result'] = true;
                $result['message'] = __('File saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('File not saved');
            }

            exit(json_encode($result));
        }
    }

    public function delete(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');

            $record = $this->Files->query();
            if($record->delete()->where(['id'=>$id])->execute()){
                $result['result'] = true;
                $result['message'] = __('File deleted');
            }else{
                $result['result'] = false;
                $result['message'] = __('File not deleted');
            }

            exit(json_encode($result));
        }        
    }

    public function getVNO(){
        $this->autoRender = false;

        if($this->request->is('post')){

            $result = array(
                'result'  => false,
                'message' => ''
            );

            $vno = $this->request->data('vno');
            $edp = $this->request->data('edp');
            $amount = $this->request->data('amount');

            if(!$vno && !$edp){
                //$result['message'] = 'Uanble to generate the Voucher Code. Please choose EDP or enter the order number.';
                //exit(json_encode($result));
                $vno = round(rand(1, 10)) . str_pad(rand(10, 10000), 5, '0', STR_PAD_LEFT);
            }

            $vno = !$vno ? $edp : $vno;

            $record = TableRegistry::get('Downloadlogs')->find('all', ['conditions'=>['voucherno LIKE'=>$vno.'%'],'order'=>['voucherno'=>'desc'] ]);

            $values = [];
            if($record->count()){
                $record = $record->first();
                $idx    = 1;
                $prefix = $record->voucherno;
                $prefix = intval(substr($prefix, strlen($vno)));

                for($i = 0 ; $i < $amount ; $i++){
                    $values[] = $vno.str_pad($prefix + 1 + $i,  5, '0', STR_PAD_LEFT);
                }
                $result['message'] = 'PENDDING';
            }else{
                for($i = 0 ; $i < $amount ; $i++){
                    $values[] = $vno.str_pad(1 + $i, 5, '0', STR_PAD_LEFT);
                }
                $result['message'] = 'NEW';
            }
            $result['result'] = $vno.' Amount:'.$amount.' generated.';

            //find the duplicate if any,
            if(!TableRegistry::get('Downloadlogs')->find('all', ['conditions'=>['voucherno'=>$vno]])->count()){
                $isrtQry = TableRegistry::get('Downloadlogs')->query();
                foreach($values as $key=>$value){
                    $isrtQry->insert(['voucherno'])->values(['voucherno'=>$value]);
                }
                $isrtQry->execute();
            }

            exit(json_encode($result));
        }
    }

    public function getAllVNO(){
        $this->autoRender = false;
        if($this->request->is('post')){
            //$this->layout = 'plain';
            $result = array(
                'result'  => false,
                'message' => ''
            );

            $vno = $this->request->data('vno');
            $edp = $this->request->data('edp');

            if(!$vno && !$edp){
                $result['message'] = 'There is no order number nor EDP.';
                exit(json_encode($result));
            }

            $vno = !$vno ? $edp : $vno;

            $records = TableRegistry::get('Downloadlogs')->find('all', ['conditions'=>['voucherno LIKE'=>$vno.'%', 'is_used'=>0],'order'=>['voucherno'=>'desc'], 'fields'=>['voucherno'], 'hydrate'=>false ]);

            if($records->count()){
                $result['result'] = '/files/'.$vno.'.csv';
            }

            $output = fopen(Configure::read('CSV_FILE_PATH').$vno.'.csv', 'w');

            $values = [];
            $records = $records->toArray();

            foreach($records as $key => $record){
                $values[] = $record['voucherno'];
            }

            fputcsv($output, $values);

            exit(json_encode($result));
        }
    }   

}//EOC

//EOF