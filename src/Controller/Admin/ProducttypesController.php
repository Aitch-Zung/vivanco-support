<?php
namespace App\Controller\Admin;

use \Cake\Event\Event as Event;
use \Cake\ORM\TableRegistry as TableRegistry;
use \Cake\Core\Configure as Configure;

class ProducttypesController extends AppController {

	public function initialize(){
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'home'
            ]
        ]);
    }

    public function index(){
        $producttypes = $this->Producttypes->find('all');

        $this->set(compact(array('producttypes')));
	}

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->layout = 'plain';
    }

    public function edit(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id      = $this->request->data('id');
            $name    = $this->request->data('name');
            $display = $this->request->data('display');
            $files   = $this->request->data('files');

            $result = array(
                'result'  => false,
                'message' => ''
            );

            $img = '';
            if(isset($files[0]) && isset($files[0]['tmp_name'])){
                $newFileName = sha1($files[0]['name']) . '_' . date('YmdHis') . '.' . $this->getExt($files[0]['name']);
                $this->shrinkImg($files[0]['tmp_name'], 442, 344);
                $response = $this->uploadToLocalServer($files[0]['tmp_name'], $newFileName);
                $img = Configure::read('IMG_ROOT') . $newFileName;
            }

            if(!$id){//CREATE
                $record = $this->Producttypes->query();
                $record->insert(['name', 'display', 'img'])->values(['name'=>$this->request->data('name'), 'display'=>$this->request->data('display'), 'img'=>$img]);
            }else{//UPDATE
                $columns = ['name', 'display'];
                $updateData = ['name'=>$this->request->data('name'), 'display'=>$this->request->data('display')];
                if($img !== ''){
                    $columns[] = 'img';
                    $updateData['img'] = $img;
                }

                $record = $this->Producttypes->query();
                $record->update($columns)->set($updateData)->where(['id' => $id]);
            }

            if($record->execute()){
                $result['result'] = true;
                $result['message'] = __('Product type saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product type not saved');
            }

            exit(json_encode($result));
        }
    }

    public function toggle(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');
            $toggle = $this->request->data('toggle');

            $record = $this->Producttypes->query();
            $record->update(['display'])->set(['display'=>$toggle])->where(['id' => $id]);

            if($record->execute()){
                $result['result'] = true;
                $result['message'] = __('Product type saved');
            }else{
                $result['result'] = false;
                $result['message'] = __('Product type not saved');
            }

            exit(json_encode($result));
        }
    }

    public function delete(){
        $this->autoRender = false;

        if($this->request->is('post')){
            $id = $this->request->data('id');

            //find the related records
            $products = TableRegistry::get('Products');
            $products = $products->find('all', ['conditions' => ['Products.type_id'=>$id] ]);
            $productCount = $products->count();

            if($productCount){//records exist, cannot be deleted
                $result['result'] = false;
                $result['message'] = __('There are {0} product records related to this type', [$productCount]);
            }else{
                $record = $this->Producttypes->query();
                if($record->delete()->where(['id'=>$id])->execute()){
                    $result['result'] = true;
                    $result['message'] = __('Product type deleted');
                }else{
                    $result['result'] = false;
                    $result['message'] = __('Product type not deleted');
                }
            }

            exit(json_encode($result));
        }        
    }
	
}