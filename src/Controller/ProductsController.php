<?php
/**
 * Product page controller
 *
 * @author Aitch.Zung <aitch.zung@icloud.com>
 * @version 1.0.0
 */
namespace App\Controller;

use \Cake\Core\Configure;
use \Cake\Network\Exception\NotFoundException;
use \Cake\View\Exception\MissingTemplateException;
use \Cake\ORM\TableRegistry as TableRegistry;

class ProductsController extends AppController {

	public function index($pagetype='index'){

		if(strtolower($pagetype) === 'index'){
			$producttypes = TableRegistry::get('Producttypes')->find('all', ['conditions' => ['Producttypes.display'=>1]]);

			$this->set(compact(array('producttypes')));
		}else{
			//load products
			$producttypes = TableRegistry::get('Producttypes')->find('all', ['conditions' => ['Producttypes.display'=>1]]);

			$productType = TableRegistry::get('Producttypes')->find('all', ['conditions' => ['Producttypes.name'=>$pagetype]])->first();

			$products = TableRegistry::get('Products')->find('all', ['conditions' => ['Products.display'=>1, 'Products.type_id'=>$productType->id]]);

			$pagetype = 'products';

			$productTypeName = $productType->name;

			$this->set(compact(array('producttypes', 'products', 'productTypeName')));
		}

		$this->render($pagetype);
		
	}//EOA

	public function read($id){
		$product = TableRegistry::get('Products')->find('all', ['conditions'=>['Products.id'=>$id]]);
		$product = $product->first();

		if($product){
			$conditions = [];
			$conditions['edp'] = $product->edp;
			$conditions['model_no'] = $product->model_no;
			$files = TableRegistry::get('Files')->find('all')->where(['OR'=>$conditions, 'AND'=>['Files.display'=>1]])->order(['counter'=>'desc']);

			$this->set('files', $files);
		}

		$this->set(compact(array('product')));
	}
	
}//EOC

//EOF