# LANGUAGE translation of CakePHP Application
# Copyright 2015 Aitch.Zung <aitch.zung@icloud.com>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2015-04-19 14:31+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/PagesController.php:54
msgid "Home"
msgstr ""

#: Controller/Admin/UsersController.php:22
msgid "Not a valid user"
msgstr ""

#: Controller/Admin/UsersController.php:24
msgid "Valid user"
msgstr ""

#: Template/Admin/Users/login.ctp:3
msgid "In order to login into the backend site, please enter your username and password below."
msgstr ""

#: Template/Admin/Users/login.ctp:7
msgid "Username"
msgstr ""

#: Template/Admin/Users/login.ctp:8
msgid "valid username here..."
msgstr ""

#: Template/Admin/Users/login.ctp:11
msgid "Password"
msgstr ""

#: Template/Admin/Users/login.ctp:12
msgid "valid password here..."
msgstr ""

#: Template/Admin/Users/login.ctp:15
msgid "Login"
msgstr ""

#: Template/Admin/Users/login.ctp:16
msgid "Prove you're not a robot, press Login btn."
msgstr ""

#: Template/Admin/Users/login.ctp:32
msgid "Please enter the username and password!"
msgstr ""

#: Template/Layout/admin.ctp:39
#: Template/Layout/default.ctp:39
msgid "Vivanco Screen Protectors."
msgstr ""

#: Template/Layout/admin.ctp:52
#: Template/Layout/default.ctp:96
msgid "Copyright &copy; 2015 Vivanco."
msgstr ""

#: Template/Layout/default.ctp:47
msgid "Product"
msgstr ""

#: Template/Layout/default.ctp:50
msgid "Downloads"
msgstr ""

#: Template/Layout/default.ctp:53
msgid "Support"
msgstr ""

#: Template/Layout/default.ctp:58
msgid "Search..."
msgstr ""

#: Template/Layout/default.ctp:81
msgid "Vivanco Screen Protectors:"
msgstr ""

#: Template/Layout/default.ctp:82
msgid "Intelligent Solutions fit with every smartphone, just follow the steps and use the received code to find the pattern of your device."
msgstr ""

#: Template/Layout/default.ctp:87
msgid "Intelligent Solutions:"
msgstr ""

#: Template/Layout/default.ctp:88
msgid "Fit with every smartphone, just follow the steps and use the received code to find the pattern of your deviceVivanco is one of Europe's leading providers of equipment and connecting accessories. As multispecialist we distribute products of the segments Consumer Electronics, Information Technology and Telecommunications."
msgstr ""

#: Template/Products/index.ctp:2
msgid "What are you searching for?"
msgstr ""

#: Template/Products/index.ctp:11
#: Template/Products/smartphones.ctp:3
#: Template/Products/tablets.ctp:3
msgid "Smartphones"
msgstr ""

#: Template/Products/index.ctp:24
#: Template/Products/smartphones.ctp:5
#: Template/Products/tablets.ctp:5
msgid "Tablets"
msgstr ""

