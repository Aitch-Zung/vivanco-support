# Vivanco Support Website

This system is built with CakePHP, please follow the instructions below to deploy the website.

DB schemas are in the db.tables.sql.

## Configuration

Read and edit `config/app.php` and setup the 'Datasources' and any other
configuration relevant for your application.