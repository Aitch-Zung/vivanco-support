console.info('hi, there');

var files = null;

$('div.error.message').html('').hide();
$('div.info.message').html('').hide();

$('div.d-content table.producttypes').on('click', 'div.action',function($event){
	var cmd = $(this).attr('cmd'),
	    id  = $(this).attr('cmdid');

	try{
	    var record = $.parseJSON($('#record-' + id).val());
	}catch(exp){
	}

	switch(cmd){
		case 'edit':
			$('#targetid').val(id);
			$('input[name=name]').val(record.name);
			$('select[name=display]').val(record.display ? "1" : "0");
			break;
		case 'toggle':
			var togglevalue = $(this).attr('togglevalue') === '1' ? '0' : '1';
			$.post('/admin/producttypes/toggle', { id : id, toggle : togglevalue }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/producttypes');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
		case 'remove':
			if(!confirm('<?= __('Are you sure?'); ?>')){
				return;
			}
			$.post('/admin/producttypes/delete', { id : id }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/producttypes');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
	}

	$event.preventDefault();
});

$('div.d-content').on('change', 'input[type=file]', function($event){
	files = $event.target.files;
});

$('div.d-content').on('click', 'div.save-btn', function($event){
	var targetid = $('#targetid').val(),
	    formData = new FormData(),
	    typename = $('input[name=name]'),
	    display  = $('select[name=display]');

	    $('div.error.message').html('').hide();
		$('div.info.message').html('').hide();
		
		if(files){
		    $.each(files, function(key, value){
		    	formData.append("files["+key+"]", value);
		    });
		}

	    if(typename.val()){
	    	formData.append('name', typename.val());
	    }

	    if(display.val()){
	    	formData.append('display', display.val());
	    }

	    if(targetid){
	    	formData.append('id', targetid);	
	    }
	    
	    $.ajax({
	    	url  : '/admin/producttypes/edit',
	    	type : 'post',
	    	data : formData,
	    	dataType : 'json',
	    	cache : false, processData : false, contentType : false,
	    	success : function(result, textStatus, jqXHR){
	    		var lastkey = parseInt($('#lastkey').val(), 10);

	    		if(result.result){
	    			
	    			$('div.d-content').addClass('loading').load('/admin/producttypes');
	    			
	    		}else{
	    			$('div.error.message').html(result.message).show();
	    		}
	    	},
	    	error : function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	}
	    });

});