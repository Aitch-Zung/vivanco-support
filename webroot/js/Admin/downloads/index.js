var hash = window.location.hash,
    file = '',
    sent = false;

hash = hash ? hash.substr(1) : false;

if(hash){
	$('.menu .item').tab('change tab', hash);
}else{
	$('.menu .item').tab();
}

$('div.downloads').on('click', 'div.menu-options span.button', function($event){
	var cmd   = $(this).attr('cmd'),
	    cmdid = $(this).attr('cmdid'),
	    $menuConyainer = $('#MenuOption' + cmdid),
	    parentid = $(this).attr('parentid') || 1;
	console.info('cmd:', cmd);
	switch(cmd){
		case 'add-option':
			$menuConyainer.append($('#AddOptionTemp').html()).find('span.button').attr({cmdid:cmdid, parentid: parentid});
			break;
		case 'add-options':
			var cmdtext = $(this).attr('cmdtext');
			$('#BulkImportTxt').html(cmdtext);
			$('#ParentId').val(parentid);
			$('#TypeId').val(cmdid);
			if(!$('div.import-modal').modal('is active')){
				$('div.import-modal').modal('show');
			}
			break;
		case 'cancel-option':
			$menuConyainer.find('div.option-editor').remove();
			break;
		case 'save-option':
			var data = {
				typeid : cmdid,
				parentid : parentid,
				name : $menuConyainer.find('div.option-editor input[name=option-name]').val()
			};
			$.post('/admin/downloads/addoption', data, function(result){
				if(result.result){
					window.location.hash = '#first';
					$('div.d-content').addClass('loading').load('/admin/downloads');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json');
			break;
		case 'delete-option':
			if(!confirm('Are you sure?')){
				return;
			}
			$.post('/admin/downloads/deloption', { id: cmdid }, function(result){
				if(result.result){
					window.location.hash = '#first';
					$('div.d-content').addClass('loading').load('/admin/downloads');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json');
			break;
	}

	$event.preventDefault();
});

//init the submenu
$.post('/admin/downloads/getsub', {parentid:$('select[name=type-1]').val() }, function(result){
	if(result){
		$('div.option-cloumn-grid div.menu-options:gt(0)').remove();
		$('div.option-cloumn-grid').append(result);
	}
});

//fetch the submenu 
$('div.downloads').on('change', 'select.dynamic-menu', function($event){
	var menuid = $(this).val(),
		level  = $(this).attr('cmdlevel') || 1;

	$.post('/admin/downloads/getsub', {parentid:menuid}, function(result){
		if(result){
			$('div.option-cloumn-grid div.menu-options:gt('+(level - 1)+')').remove();
			$('div.option-cloumn-grid').append(result);
		}
	});

	$event.preventDefault();
});

$('div.import-modal input[name=csv]').on('change', function($event){
	file = $event.target.files[0];
});

$('#ImportBtn').on('click', function($event){
	var parentid = $('#ParentId').val(),
		typeid   = $('#TypeId').val(),
		formData = new FormData();

	if(!parentid){
		console.error('no parentid');
		return;
	}

	if(file !== ''){
	    formData.append("csv", file);
	    file = '';
	}else{
		console.error('no csv file');
		return;
	}

	formData.append("parentid", parentid);
	formData.append("typeid",   typeid);

	$('div.import-modal').remove();

	if(!sent){
		$.ajax({
	    	url  : '/admin/downloads/bulkimport',
	    	type : 'post',
	    	data : formData,
	    	dataType : 'json',
	    	cache : false, processData : false, contentType : false,
	    	success : function(result, textStatus, jqXHR){
	    		if(result.result){
	    			$('div.d-content').addClass('loading').load('/admin/downloads');
	    		}else{
	    			$('div.error.message').html(result.message).show();
	    		}

	    		sent = false;
	    	},
	    	error : function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	}
	    });
	}	

	sent = true;
	$event.preventDefault();
});