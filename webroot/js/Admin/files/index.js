var files = null, 
	sent  = false;

$('div.error.message').html('').hide();

$('div.d-content table.files').on('click', 'div.action',function($event){
	var cmd = $(this).attr('cmd'),
	    id  = $(this).attr('cmdid');

	try{
	    var record = $.parseJSON($('#record-' + id).val());
	}catch(exp){
		console.info($('#record-' + id).val());
		console.info('Parse JSON errors:', exp);
	}

	switch(cmd){
		case 'edit':
			$('#targetid').val(id);
			$('div.form.files input[name=voucherno]').val(record.voucherno);
			$('div.form.files input[name=name]').val(record.name);
			$('div.form.files select[name=model_no]').val(record.model_no);
			$('div.form.files select[name=edp]').val(record.edp);
			$('div.form.files select[name=display]').val(record.display ? "1" : "0");
			$('div.form.files select[name=extsearch1]').val(record.extsearch1);
			$('div.form.files select[name=extsearch2]').val(record.extsearch2);
			$('div.form.files select[name=extsearch3]').val(record.extsearch3);
			$('span.modemsg').html('[Edit]');
			break;
		case 'toggle':
			var togglevalue = $(this).attr('togglevalue') === '1' ? '0' : '1';
			$.post('/admin/files/toggle', { id : id, toggle : togglevalue }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/files');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
		case 'remove':
			if(!confirm('Are you sure?')){
				return;
			}
			$.post('/admin/files/delete', { id : id }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/files');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
		case 'get-vno':
			var amount = parseInt(prompt('How many numbers you want to generate?'), 10);
			if(isNaN(amount) || amount <= 0){
				return;
			}

			$('table.table.files').addClass('loading');

			$.post('/admin/files/getVNO', { vno : record.voucherno, amount: amount, edp : record.edp }, function(result){
				if(result.result){
					$('span#VoucherNo' + record.id).html('Voucher Code: &nbsp;&nbsp;' + result.result).show();
				}else{
					$('div.error.message').html(result.message).show();
				}
				$('table.table.files').removeClass('loading');
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
				$('table.table.files').removeClass('loading');
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
		case 'get-all-vno':
			$.post('/admin/files/getAllVNO', { vno : record.voucherno, edp : record.edp }, function(result){
				if(result.result){
					$('span#VoucherNo' + record.id).html('<a href="'+result.result+'" target="_blank"><i class="icon download"></i>Voucher Code File</a>').show();
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
			break;
	}

	$event.preventDefault();
});

$('div.d-content').on('change', 'input[type=file]', function($event){
	files = $event.target.files;
});

$('div.d-content').on('click', 'div.files div.save-btn', function($event){
	var formData    = new FormData(),
		targetid    = $('#targetid').val(),
		filename    = $('input[name=name]'),
	    display     = $('select[name=display]'),
	    modleno	    = $('select[name=model_no]'),
	    edp         = $('select[name=edp]'),
	    extsearch1  = $('select[name=extsearch1]'),
	    extsearch2  = $('select[name=extsearch2]'),
	    extsearch3  = $('select[name=extsearch3]'),
	    voucherno   = $('input[name=voucherno]');

	    $('div.error.message').html('').hide();
		
		if(files){
		    $.each(files, function(key, value){
		    	formData.append("files["+key+"]", value);
		    });
		}

	    if(filename.val()){
	    	formData.append('name', filename.val());
	    }

	    if(display.val()){
	    	formData.append('display', display.val());
	    }

	    if(modleno.val()){
	    	formData.append('model_no', modleno.val());
	    }

	    if(edp.val()){
	    	formData.append('edp', edp.val());
	    }

	    if(extsearch1.val()){
	    	formData.append('extsearch1', extsearch1.val());
	    }

	    if(extsearch2.val()){
	    	formData.append('extsearch2', extsearch2.val());
	    }

	    if(extsearch3.val()){
	    	formData.append('extsearch3', extsearch3.val());
	    }

	    if(voucherno.val()){
	    	formData.append('voucherno', voucherno.val());
	    }

	    if(targetid){
	    	formData.append('id', targetid);	
	    }
	    
	    if(!sent){
	    	$('div.d-content').addClass('loading');
	    	$(this).addClass('loading disabled');
		    $.ajax({
		    	url  : '/admin/files/upload',
		    	type : 'post',
		    	data : formData,
		    	dataType : 'json',
		    	cache : false, processData : false, contentType : false,
		    	success : function(result, textStatus, jqXHR){
		    		var lastkey = parseInt($('#lastkey').val(), 10);
		    		$('div.d-content').removeClass('loading');
		    		$(this).removeClass('loading disabled');
		    		if(result.result){
		    			$('div.d-content').addClass('loading').load('/admin/files');
		    		}else{
		    			$('div.error.message').html(result.message).show();
		    		}
		    		sent = false;
		    	},
		    	error : function(jqXHR, textStatus, errorThrown){
		    		$('div.error.message').html('ERRORS: ' + textStatus).show();
		    	}
		    });
		}

	    sent = true;

});

$('div.dynamic-fields').on('change', 'select.dynamic-menu', function($event){
	var value = $('option:selected', this).attr('cmdid'),
	    level  = $(this).attr('cmdlevel') || 1;
	if(value){
		$.post('/admin/downloads/getsub/plain', {parentid:value }, function(result){
			if(result){
				$('div.dynamic-fields select.dynamic-menu:gt('+(level - 1)+')').parents('div.field.menu-options').remove();
				$('div.dynamic-fields').append(result);
			}
		});
	}
	$event.preventDefault();
});

$('.help-popup').popup({
	inline   : true,
	hoverable: true,
	position : 'top right',
	delay: {
	  show: 300,
	  hide: 800
	}
});

$('#HelpBtn').click(function(){
	$('.help-modal').modal('show')
});