var files = [],
	gfiles = [], 
	sent  = false;

$('div.error.message').html('').hide();
$('div.info.message').html('').hide();

$('div.d-content table.products').on('click', 'div.action',function($event){
	var cmd = $(this).attr('cmd'),
	    id  = $(this).attr('cmdid');

	try{
	    var record = $.parseJSON($('#record-' + id).val());
	}catch(exp){
		console.info('Parse JSON errors:', exp);
	}

	switch(cmd){
		case 'edit':
			$('#targetid').val(id);
			$('div.form.products input[name=name]').val(record.model_no);
			$('div.form.products input[name=color]').val(record.color);
			$('div.form.products input[name=description]').val(record.description);
			$('div.form.products input[name=edp]').val(record.edp);
			$('div.form.products select[name=display]').val(record.display ? "1" : "0");
			$('div.form.products select[name=type_id]').val(record.type_id);
			$('textarea[name=details]').val(record.details);
			$('span.modemsg').html('[Edit]');
			$('html, body').animate({
        		scrollTop: $('#EditProductForm').offset().top
    		}, 500);
			break;
		case 'toggle':
			var togglevalue = $(this).attr('togglevalue') === '1' ? '0' : '1';
			$.post('/admin/products/toggle', { id : id, toggle : togglevalue }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/products');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
		case 'remove':
			if(!confirm('Are you sure?')){
				return;
			}
			$.post('/admin/products/delete', { id : id }, function(result){
				if(result.result){
					$('div.d-content').addClass('loading').load('/admin/products');
				}else{
					$('div.error.message').html(result.message).show();
				}
			}, 'json').error(function(jqXHR, textStatus, errorThrown){
	    		$('div.error.message').html('ERRORS: ' + textStatus).show();
	    	});
			break;
	}

	$event.preventDefault();
});

$('div.d-content').on('change', 'input#MainPhoto', function($event){
	files.push($event.target.files[0]);
});

$('div.d-content').on('change', 'input[name=gfile]', function($event){
	var id = $(this).attr('id');
	gfiles.push({ key : id, fv: $event.target.files[0] });
});

$('div.d-content').on('click', 'i.remove-img', function($event){
	$event.preventDefault();
	var cmd = $(this).attr('cmd'),
	    cmdid = $(this).attr('cmdid'),
	    $removebtn = $(this);

	$.post('/admin/products/removeimg', { id : cmd, idx : cmdid }, function(result){
		$removebtn.prev('img.ui.image.tiny').remove();		
		$removebtn.remove();
	});
});

$('div.d-content').on('click', 'div.products div.save-btn', function($event){
	var formData    = new FormData(),
		targetid    = $('#targetid').val(),
	    typename    = $('input[name=name]'),
	    display     = $('select[name=display]'),
	    typeid	    = $('select[name=type_id]'),
	    edp         = $('input[name=edp]'),
	    color       = $('input[name=color]'),
	    description = $('input[name=description]'),
	    details     = $('textarea[name=details]');

	    $('div.error.message').html('').hide();
		$('div.info.message').html('').hide();
		
		if(files && files.length && !sent){
		    $.each(files, function(key, value){
		    	formData.append("files["+key+"]", value);
		    });
		    files = [];
		}

		//console.info('gfiles:',gfiles, gfiles.length);
		if(gfiles && gfiles.length){
			$.each(gfiles, function(key, value){
				console.info(key, value);
		    	formData.append(value.key, value.fv);
		    });
		    gfiles = [];
		}

	    if(typename.val()){
	    	formData.append('name', typename.val());
	    }

	    if(display.val()){
	    	formData.append('display', display.val());
	    }

	    if(typeid.val()){
	    	formData.append('typeid', typeid.val());
	    }

	    if(edp.val()){
	    	formData.append('edp', edp.val());
	    }

	    if(color.val()){
	    	formData.append('color', color.val());
	    }

	    if(description.val()){
	    	formData.append('description', description.val());
	    }

	    if(details.val()){
	    	formData.append('details', details.val());
	    }

	    if(targetid){
	    	formData.append('id', targetid);	
	    }
	    
	    if(!sent){
	    	$(this).addClass('loading disabled');

		    $.ajax({
		    	url  : '/admin/products/edit',
		    	type : 'post',
		    	data : formData,
		    	dataType : 'json',
		    	cache : false, processData : false, contentType : false,
		    	success : function(result, textStatus, jqXHR){
		    		var lastkey = parseInt($('#lastkey').val(), 10);

		    		$(this).removeClass('loading disabled');

		    		if(result.result){
		    			$('div.d-content').addClass('loading').load('/admin/products');
		    		}else{
		    			$('div.error.message').html(result.message).show();
		    		}

		    		sent = false;
		    	},
		    	error : function(jqXHR, textStatus, errorThrown){
		    		$(this).removeClass('loading disabled');
		    		$('div.error.message').html('ERRORS: ' + textStatus).show();
		    	}
		    });
		}

	    sent = true;

});
