var files = null,
	sent  = false;

$('div.error.message').html('').hide();
$('div.info.message').html('').hide();

$('div.d-content').on('change', 'input[type=file]', function($event){
	files = $event.target.files;
});

$('div.settings div.save-btn').on('click', function($event){

	var cmd = $(this).attr('cmd'),
		formData = new FormData(),
		btn = this;

	$('div.error.message').html('').hide();
	$('div.info.message').html('').hide();

	switch(cmd){
		case 'change-password':
			var newPassword = $('input[name=newpassword]').val(),
				conPassword = $('input[name=conpassword]').val(),
				oldPassword = $('input[name=oldpassword]').val();

			if(newPassword !== conPassword){
				$('div.error.message').html('New passwords don\'t match, please confirm again.').show();
				return;
			}else{
				$(btn).addClass('loading disabled');
				if(!sent){
					$.post('/admin/settings/modpass', { oldpassword: oldPassword, newpassword: newPassword }, function(result){
						$(btn).removeClass('loading disabled');
						if(result.result){
							$('div.info.message').html('Password changed').show();
						}else{
							$('div.error.message').html(result.message).show();
						}
						sent = false;
					}, 'json');
					sent = true;
				}
			}
			break;
		case 'chnage-home-img':
		case 'chnage-support-img':

			if(files){
			    $.each(files, function(key, value){
			    	formData.append("files["+key+"]", value);
			    });
			}

			formData.append('cmd', cmd);

			if(!sent){

		    	$(btn).addClass('loading disabled');

			    $.ajax({
			    	url  : '/admin/settings/modset',
			    	type : 'post',
			    	data : formData,
			    	dataType : 'json',
			    	cache : false, processData : false, contentType : false,
			    	success : function(result, textStatus, jqXHR){
			    		var lastkey = parseInt($('#lastkey').val(), 10);

			    		$(btn).removeClass('loading disabled');

			    		if(result.result){
			    			
			    			$('div.info.message').html('Setting changed').show();
			    			
			    		}else{
			    			$('div.error.message').html(result.message).show();
			    		}

			    		sent = false;
			    	},
			    	error : function(jqXHR, textStatus, errorThrown){
			    		$('div.error.message').html('ERRORS: ' + textStatus).show();
			    	}
			    });

			    sent = true;
			}

			break;
	}

	$event.preventDefault();
});
