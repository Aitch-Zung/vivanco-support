"use strict"

$(document).ready(function(){

	var controller = $('#controller').val(),
		action     = $('#action').val();

	$('.ui.dropdown').dropdown();
	$('.menu .item').tab();
	
	$('a.tab-menu').on('click', function($event){
		var href = $(this).attr('href');
		
		location.href = href;

		$event.preventDefault();
	});

	$('div.sub-tab-menu').on('click', function($event){
		var href = $(this).attr('cmd');

		if(href !== null && href !== undefined && href !== ''){
			location.href = href;
		}

		$event.preventDefault();
	});

	$('img.lang-flag').on('click', function($event){
		var lang = $(this).attr('cmd');

		$.get('/Languages/setLang/'+lang, function(result){
			$('#content').addClass('ui form loading');
			location.reload();
		}, 'json');
		$event.preventDefault;
	});

	//change tab active status if any
	$('a[data-tab='+controller+']').addClass('active');

	//trigger event to notify the listeners to start
	$(this).trigger('geh-zur-arbeit');
});